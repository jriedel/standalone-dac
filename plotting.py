import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.colors as colors
from matplotlib.animation import FuncAnimation
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import pandas as pd
import seaborn as sns
import squarify
import xarray as xr

import classification as cfc
import gis
import helper_functions as hf
import transport_storage as ts


basemap_color = "#8B4513"
basemap_alpha = 0.4

def timeseries():
    """
    Shows smoothing of generation profile by battery at a certain location

    NOTE: function was used in console, where p is selected point, created using p = self.create_example_point()
    of a CostCalulation object + by importing battery_smoothing as in cost_calculations.py in the console

    """

    for item in [0.2, 0.3, 0.4]:
        plt.plot(np.arange(500), p["e_out"].isel(hourofyear=slice(500)).data, linewidth=6, color="Blueviolet")
        plt.plot(np.arange(500), battery_smoothing(p["e_out"].isel(hourofyear=slice(500)).data, item, 1, 0.98, 0.98)[0],
                 color="Orange")
        plt.show()


# define plot params
# plt.rcParams['figure.figsize'] = (8, 14)
plt.rcParams.update({'font.size': 14})
# matplotlib.use('TkAgg')

crs = 3035
xlim = np.array([2500000, 6600000])
ylim = np.array([1400000, 5500000])

# DataFrames of Europe and World
world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres')).to_crs(crs)
fltr_world = world['name'].isin(['Russia', 'Belarus', 'Ukraine', 'Moldova',
                                 'Morocco', 'Algeria', 'Tunisia', 'Iraq', 'Iran',
                                 'Azerbaijan', 'Georgia', 'Syria', 'Armenia'
                                 'Jordan', 'Israel', 'Saudi Arabia', 'Turkey'])

world = world.loc[fltr_world]
europe = gpd.read_file(r"processed_data/maps/europe_clip_3035_wo_Turkey.shp")

# cmaps
d = pd.read_excel("raw_data/tables/cmaps.xlsx", index_col=0).to_dict("list")
cmaps = dict([k, [item for item in l if not pd.isnull(item) == True]] for k, l in d.items())


def subplot_array(dim, images):
    plt.figure()
    rows, cols = dim

    fig, ax = plt.subplots(nrows=rows, ncols=cols)

    for i in range(rows * cols):
        ax[i].imshow(images[i])
    plt.show()


def grid_plot(shape, grid):
    fig, ax = plt.subplots(figsize=(6, 6))
    grid.plot(ax=ax, edgecolor="grey", color="None")
    shape.plot(ax=ax)
    plt.show()


def plot_xy_df(df, column, crs=3035, res=0.25):
    gdf = gis.df_xy_to_gdf(df, res).to_crs(crs)

    gdf.plot(column=column,
             legend=True,
             legend_kwds={'label': f"Column {column}"})
    plt.show()


def animate_scatter_xy(data, xlim, ylim):
    """
    Animation of PSO convergence
    """
    matplotlib.use('TkAgg')
    fig, ax = plt.subplots()

    def animate(t):
        ax.clear()
        ax.set_xlim(*xlim)
        ax.set_ylim(*ylim)
        ax.scatter(data[t][:, 0], data[t][:, 1])
        ax.set_title(f"Iteration {t}")
        ax.set_xlabel("ratio PV:wind capacity")
        ax.set_ylabel("ratio DAC:RET capacity")

    ani = FuncAnimation(fig, animate, frames=len(data), interval=100)

    ani.save("23-04-25 UK.gif", writer="Pillow")

    plt.show()


def animate_map(ds, interval, save_path=False):
    matplotlib.use('TkAgg')
    fig, ax = plt.subplots()

    time_dim = [var for var in ds.coords if "year" in var][0]
    frames = ds[time_dim].size

    def animate(time):
        ax.clear()
        ds.isel({time_dim: time}).plot(ax=ax, add_colorbar=False, cmap="Spectral_r")

    ani = FuncAnimation(fig, animate, frames=frames, interval=interval, blit=False)

    plt.show()

    if save_path:
        ani.save(save_path, writer="Pillow")


def create_alpha_cmap(cmap):
    """ Create colormap with transparent 'bottom' color"""
    ncolors = 256
    color_array = plt.get_cmap(cmap)(range(ncolors))
    color_array[:, -1] = np.linspace(0.0, 1.0, ncolors)

    map_object = LinearSegmentedColormap.from_list(name=f"{cmap}_alpha", colors=color_array)
    matplotlib.colormaps.register(cmap=map_object)

    return plt.get_cmap(f"{cmap}_alpha")


def europe_map(dataset,
               variable,
               x_label=' ',
               y_label=' ',
               cbar_label=' ',
               title=' ',
               colormap="Spectral_r",
               frame_ratio=0,
               discrete_cbar=False,
               alpha=False,
               savefig_path=False,
               ax=None,
               vmin=None,
               vmax=None,
               norm=None,
               cbar=True,
               masked=False,
               pad=0.1):

    cmap_list = []
    cbar_kwargs = {"label": cbar_label}
    levels = None

    if masked:
        dataset[variable] = dataset.where(dataset["land_pv"] != 0)[variable]

    if not colormap:
        cmap_list = hf.get_key_from_string(cmaps, variable)
        colormap = np.random.choice(cmap_list)

    if alpha:
        colormap = plt.get_cmap(f"{colormap}_alpha")

    if discrete_cbar:
        vals = np.unique(dataset[variable].data[~np.isnan(dataset[variable].data)])
        min_, max_, nums = vals.min(), vals.max(), vals.size
        bounds = np.linspace(min_, max_, nums)
        levels = max_ - min_
        colormap = getattr(matplotlib.cm, colormap)
        colormap = plt.get_cmap(colormap, nums+1)
        cbar_kwargs.update({"spacing": "proportional", "ticks": bounds})

    if not ax:
        fig, ax = plt.subplots(figsize=(10, 10))

    ax.tick_params(left=False, right=False, top=False, bottom=False,
                   labelleft=False, labelbottom=False,
                   )

    divider = make_axes_locatable(ax)

    world.plot(ax=ax, color="grey", alpha=0.3, edgecolor="grey")
    europe.boundary.plot(ax=ax, edgecolor="black", linewidth=1)

    if len(list(dataset[variable].coords)) > 2:
        dataset[variable] = dataset[variable].drop_vars(
            [x for x in dataset[variable].coords if x not in ["x", "y"]]).squeeze()

    if not (vmin or vmax) and (norm is None):
        # define colormap range via 1% and 99% quantile of raw data
        raw_data = dataset[variable].data.flatten()
        raw_data = raw_data[~np.isnan(raw_data)]
        # vmin, vmax = np.quantile(raw_data, 0.01), np.quantile(raw_data, 0.99)

    cax = divider.append_axes("right", size="5%", pad=pad)

    if cbar:
        dataset[variable].plot.pcolormesh(ax=ax,
                                          cmap=colormap,
                                          levels=levels,
                                          cbar_ax=cax,
                                          cbar_kwargs=cbar_kwargs,
                                          shading="auto",
                                          norm=norm,
                                          vmin=vmin,
                                          vmax=vmax)
    else:
        cax.axis('off')
        dataset[variable].plot.pcolormesh(ax=ax,
                                          cmap=colormap,
                                          levels=levels,
                                          shading="auto",
                                          norm=norm,
                                          add_colorbar=False)

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)


    if savefig_path:
        plt.savefig(savefig_path)

    #plt.show()

    return cmap_list

def plot_basemap(color=basemap_color, ax=None, yticks_right=False, boundary=True):
    if ax is None:
        fig, ax = plt.subplots(figsize=(10, 10))
    ax.tick_params(left=False, right=yticks_right, top=False, bottom=False,
                   labelleft=False, labelbottom=False,
                   )

    world.plot(ax=ax, color="grey", alpha=0.3, edgecolor="grey")
    europe.plot(ax=ax, alpha=basemap_alpha, color=color, edgecolor="black", zorder=0)
    if boundary:
        europe.boundary.plot(ax=ax, alpha=0.8, edgecolor="black", linewidth=0.5, zorder=1)

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    return ax


def europe_overlay(data_array,
                   variables,
                   x_label=' ',
                   y_label=' ',
                   cbar_labels=None,
                   title=' ',
                   colormaps=None,
                   turkey=False,
                   frame_ratio=0.05,
                   discrete_cbar=False,
                   savefigpath=None):
    """
    plot of two overlayed layers with transparent bottom color on the same map (i.e. land_pv and land_wind)
    """

    #frame_x, frame_y = [np.diff(xlim), np.diff(ylim)] * np.array([-1, 1]) * frame_ratio
    #x_lim, y_lim = xlim + frame_x, ylim + frame_y
    euro = europe
    cbar_kwargs = [{"label": label} for label in cbar_labels]

    fig, ax = plt.subplots(figsize=(10, 10))
    ax.tick_params(left=False, right=False, top=False, bottom=False,
                   labelleft=False, labelbottom=False,
                   )

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)

    plt.xlim(xlim)
    plt.ylim(ylim)
    divider = make_axes_locatable(ax)

    world.plot(ax=ax, color="grey", alpha=0.3, edgecolor="grey")
    europe.boundary.plot(ax=ax, edgecolor="black")
    #europe.plot(ax=ax, color=basemap_color)
    cax_pads = [0.1, 0.8, 0.8, 0.8, 0.8]

    for idx, v in enumerate(variables):
        cax = divider.append_axes("right", size="5%", pad=cax_pads[idx])

        if len(list(data_array[v].coords)) > 2:
            data_array[v] = data_array[v].drop_vars(
                [x for x in data_array[v].coords if x not in ["x", "y"]]).squeeze()

        try:
            cmap_alpha = plt.get_cmap(f"{colormaps[idx]}_alpha")

        except ValueError:
            cmap_alpha = create_alpha_cmap(colormaps[idx])

        data = data_array[v].data.flatten()
        data_array[v].plot.pcolormesh(ax=ax,
                                      cmap=cmap_alpha,
                                      cbar_ax=cax,
                                      cbar_kwargs=cbar_kwargs[idx],
                                      add_labels=False)  # np.percentile(data[~np.isnan(data)], 50)
    if savefigpath:
        plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\pv_wind_potential.pdf",
                bbox_inches="tight")

    fig.tight_layout()
    plt.show()


def boundary(gdf):
    """ boundary plot of Europe countries' shapes"""
    plt.rcParams['figure.figsize'] = (15, 15)
    fig, ax = plt.subplots()
    gdf.boundary.plot(color="white", ax=ax, linewidth=0.5)
    ax.set_facecolor("black")
    plt.savefig(
        r'C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\presentations\maps and plots\europe_boundary_300.png',
        dpi=300)


def elbow_plot(n_cluster, inertia):
    plt.plot(n_cluster, inertia)
    #plt.suptitle(f"Elbow method k-means clustering")
    #plt.title("Variables: pv/wind generation, humidity and temperature timeseries, sample size = 200", fontsize=8)
    plt.xlabel("number of clusters")
    plt.ylabel("inertia")
    # plt.rcParams['figure.figsize'] = (8, 14)
    #matplotlib.use
    plt.show()


def boxplot(dataset, approximations, control, labels=None):
    data = cfc.relative_deviation(dataset, control, approximations)

    fig, ax = plt.subplots()
    ax.boxplot(data)

    if not labels:
        labels = approximations

    ax.set_xlabel("Classification methods")
    ax.set_ylabel("Deviation")
    ax.set_xticklabels(labels, rotation=45)

    plt.show()


def violin_deviation_plot_list(dataset, control, n_cluster, approximations=None):
    if approximations is None:
        approximations = list(set(dataset.data_vars).difference([control]))
    else:
        approximations = [full_name for short_name in approximations
                          for full_name in list(dataset.data_vars) if short_name.lower() in full_name.lower()]

    d = len(np.unique([s[-3:] for s in approximations]))

    rename_dict = {"quantiles": "Q",
                   "None": "RAW",
                   "PCA": "PCA",
                   "fft": "FT",
                   "monthly_mean": "MM",
                   "sort": "DC",
                   "statistical_measures": "MSM"}

    if isinstance(n_cluster, list):
        c = len(n_cluster)
        mean_dev = np.zeros((c, d))
        std_dev = np.zeros((c, d))
        fig, ax = plt.subplots(c, 1, sharex="all")

        for i, n in enumerate(n_cluster):
            subset = np.sort([item for item in approximations if f"_{n}_" in item])
            names = [val for k in subset for key, val in rename_dict.items() if key.lower() in k.lower()]
            deviations = cfc.relative_deviation(dataset, control, approximations=subset)
            mean_dev[i, :] = deviations.mean(axis=1)
            std_dev[i, :] = deviations.std(axis=1)
            dev = pd.DataFrame(deviations, names)
            sns.violinplot(data=dev.T, ax=ax[i], label=approximations)
            # ax[i].boxplot(deviations.T)
            ax[i].plot(np.linspace(-0.5, d - 0.5), np.zeros(50), color="black")
            ax[i].set_ylim(-0.5, 0.5)
            ax[i].set_xlim(-0.5, d - 0.5)

        fig.tight_layout()
        plt.subplots_adjust(hspace=0.25)
        plt.show()

    else:
        fig, ax = plt.subplots()
        subset = np.sort([item for item in approximations if f"_{n_cluster}_" in item])
        names = [val for k in subset for key, val in rename_dict.items() if key.lower() in k.lower()]
        deviations = cfc.relative_deviation(dataset, control, approximations=subset)
        mean_dev = deviations.mean(axis=1)
        std_dev = deviations.std(axis=1)
        dev = pd.DataFrame(deviations, names)
        sns.violinplot(data=dev.T, ax=ax, label=approximations)
        # ax[i].boxplot(deviations.T)
        ax.plot(np.linspace(-0.5, d - 0.5), np.zeros(50), color="black")
        ax.set_ylim(-0.5, 0.5)
        ax.set_xlim(-0.5, d - 0.5)

        fig.tight_layout()
        plt.show()


def violin_plot(dataset, control, approximations=None):
    if approximations is None:
        approximations = list(set(dataset.data_vars).difference([control]))
    #else:
        #approximations = [full_name for short_name in approximations
        #                   for full_name in list(dataset.data_vars) if short_name.lower() in full_name.lower()]

    rename_dict = {"None": "RAW",
                   "PCA": "PCA",
                   "fft": "FT",
                   "weekly_mean": "WM",
                   "monthly_mean": "MM",
                   "sort": "DC",
                   "statistical_measures": "SM"}

    fig, ax = plt.subplots(figsize=(10, 5))
    d = len(approximations)
    print(approximations)

    names = [val for k in approximations for key, val in rename_dict.items() if key.lower() in k.lower()]
    deviations = cfc.relative_deviation(dataset, control, approximations=approximations)
    mean_dev = deviations.mean(axis=1)
    std_dev = deviations.std(axis=1)
    dev = pd.DataFrame(deviations, names)
    sns.violinplot(data=dev.T, ax=ax, label=names)
    #ax.boxplot(deviations.T)
    ax.plot(np.linspace(-0.5, d - 0.5), np.zeros(50), color="black")
    ax.set_ylim(-0.38, 0.42)
    ax.set_xlim(-0.5, d - 0.5)
    ax.set_ylabel("Relative deviation in LCOD")
    fig.tight_layout()
    plt.subplots_adjust(hspace=0.25)
    plt.show()

def plot_network(graph):
    x = [point.x for point in graph.nodes()]
    y = [point.y for point in graph.nodes()]

    for edge in graph.edges():
        start_point, end_point = edge
        x_values = [start_point.x, end_point.x]
        y_values = [start_point.y, end_point.y]
        plt.plot(x_values, y_values, alpha=0.4)

    plt.show()


def get_color_list(number, colormap="jet"):
    cmap = getattr(plt.cm, colormap)
    return cmap(np.linspace(0, 1, number))


def transport_cost(distance=2000):
    df = pd.read_excel("raw_data/tables/transport_cost_params.xlsx", index_col=0)
    modes = ["ship_dedicated_15", "ship_container",
             "rail_dedicated", "rail_container",
             "truck_dedicated", "truck_container"]

    x = np.arange(distance)
    colors = get_color_list(len(modes))
    fig, ax = plt.subplots()

    for idx, mode in enumerate(modes):
        p = df.loc[mode]
        y = (p["a1"] + p["a2"] / x) * x
        ax.plot(x, y, color=colors[idx], label=mode)

    ax.legend()
    plt.show()


def cost_quantity_plot(dataset, countries=None, country_data_var="country_idx", legend=False,
                       ax=None, color=None, linestyle="-", return_instant=False):
    if countries is None:
        countries = list(europe["index"])

    idx_dict = dict(zip(europe.index, europe["index"]))
    country_indices = [hf.get_key(idx_dict, country) for country in countries]

    for idx in country_indices:
        carbon_captured = dataset.where(dataset[country_data_var] == idx)["carbon_captured"].data.squeeze()
        lcod = dataset.where(dataset[country_data_var] == idx)["LCOD"].data.squeeze()
        mask = ~np.isnan(lcod)
        lcod = lcod[mask].flatten()
        cum_carbon = pd.Series(carbon_captured[mask].flatten()[np.argsort(lcod)]).cumsum().values
        sorted_lcod = np.sort(lcod)

        if return_instant:
            return cum_carbon, sorted_lcod

        if ax is None:
            plt.plot(cum_carbon, sorted_lcod, label=idx_dict[idx])
        else:
            ax.plot(cum_carbon, sorted_lcod, label=idx_dict[idx], color=color, linestyle=linestyle)
        if legend:
            plt.legend()

    #plt.show()


def storage_plot(thresh):
    storages = ts.get_storage_sites(thresh)
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.tick_params(left=False, right=False, top=False, bottom=False,
                   labelleft=False, labelbottom=False,
                   )

    plt.xlim(xlim)
    plt.ylim(ylim)

    world.plot(ax=ax, color="grey", alpha=0.3, edgecolor="grey")
    europe.boundary.plot(ax=ax, edgecolor="black")
    storages.plot(markersize=storages["TOTAL_STOR"] / 1e2, ax=ax, color="Red")
    plt.show()

def transport_cost_params(d, mass=1e6):
    plt.rcParams.update({'font.size': 14})
    x = np.linspace(1, d * 1000, 500)
    truck = ts.transport_cost(x, "truck_container", "linear_offset")
    rail = ts.transport_cost(x, "rail_container", "linear_offset")
    ship = ts.transport_cost(x, "ship_container", "linear_offset")
    pip_on = ts.transport_cost(x, "pipeline_dense_onshore", "fit", mass)
    pip_off = ts.transport_cost(x, "pipeline_dense_offshore", "fit", mass)
    pip_on_high = ts.transport_cost(x, "pipeline_dense_onshore", "fit", mass / 10)
    pip_off_high = ts.transport_cost(x, "pipeline_dense_offshore", "fit", mass / 10)

    modes = np.array([ship, truck, rail, pip_on, pip_off, pip_on_high, pip_off_high])  # / (x / 1000)
    color_list = ["cornflowerblue", "firebrick", "forestgreen", "grey", "lightsteelblue"]
    label_list = ["Ship", "Truck", "Train", "Pipeline Onshore", "Pipeline Offshore", "", ""]
    style_list = ["-"] * 5 + ["."] * 2
    color_list += color_list[-2:]

    for i in range(len(modes)):
        plt.plot(x / 1000, modes[i], label=label_list[i], color=color_list[i], linestyle=style_list[i], linewidth=1)
    plt.legend()
    plt.xlabel("Distance [km]")
    plt.ylabel("Unitary cost [€/tCO₂/km]")
    plt.xlim([0, 500])
    # plt.ylim([0, 0.5])


def shore_plot_graph():
    """
    example plot of method -> here: zoom-in on France
    :return:
    """
    nodes = gpd.read_file("processed_data/plotting/transport/FR_nodes.shp")
    old_nodes = gpd.read_file("processed_data/plotting/transport/FR_old_nodes.shp")
    rails = gpd.read_file("processed_data/plotting/transport/FR_rail_network.shp")
    ship = gpd.read_file("processed_data/plotting/transport/FR_ship_network.shp")
    shore = gpd.read_file("processed_data/plotting/transport/FR_shore.shp")
    links = gpd.read_file("processed_data/plotting/transport/FR_weighted_connection.shp")

    fig, ax = plt.subplots(1, 2, figsize=(10, 5), gridspec_kw={"wspace": 0.05})
    plt.subplots_adjust(hspace=0.01)
    # fig.tight_layout()
    shore.plot(ax=ax[0], color=basemap_color, alpha=basemap_alpha, zorder=0)
    shore.boundary.plot(ax=ax[0], linewidth=1, color="black", zorder=1)
    rails.plot(ax=ax[0], color="black", linewidth=2, linestyle="--", alpha=1, zorder=2)
    ship.plot(ax=ax[0], color="blue", linewidth=5, alpha=0.5, zorder=3)
    old_nodes.plot(ax=ax[0], color="cornflowerblue", edgecolor="black", alpha=1, zorder=4, markersize=40)
    nodes.plot(ax=ax[0], color="Red", alpha=1, edgecolor="black", zorder=5, markersize=40)
    ax[0].set_xlim([3.187e6, 3.5e6])
    ax[0].set_ylim([2.67e6, 2.96e6])
    ax[0].tick_params(left=False, right=False, top=False, bottom=False, labelleft=False, labelbottom=False)
    shore.plot(ax=ax[1], color=basemap_color, alpha=basemap_alpha, zorder=0)
    shore.boundary.plot(ax=ax[1], linewidth=1, color="black", zorder=1)
    rails.plot(ax=ax[1], color="black", linewidth=2, linestyle="--", alpha=1, zorder=2)
    ship.plot(ax=ax[1], color="blue", linewidth=5, alpha=0.5, zorder=3)
    links.plot(ax=ax[1], color="blue", linewidth=5, zorder=4)
    nodes.plot(ax=ax[1], color="mediumorchid", edgecolor="black", alpha=1, zorder=5, marker='s', markersize=60)
    ax[1].set_xlim([3.187e6, 3.5e6])
    ax[1].set_ylim([2.67e6, 2.96e6])
    ax[1].tick_params(left=False, right=False, top=False, bottom=False, labelleft=False, labelbottom=False)

    legend_dict = {"cornflowerblue": "Ports", "Red": "Rail Terminal"}  # "mediumorchid": "Merged Node"

    # Create a legend with custom text and marker
    legend_elements = [plt.Line2D([0], [0], marker="o", color="w", markerfacecolor=color, markersize=10, label=text,
                                  markeredgecolor='k') for color, text in legend_dict.items()]
    legend_elements.append(
        plt.Line2D([0], [0], marker="s", color="w", markerfacecolor="mediumorchid", markersize=10, label="Merged Node",
                   markeredgecolor='k'))
    ax[0].legend(handles=legend_elements, loc='lower left', fontsize="small")

    plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\missing_links.pdf",
                bbox_inches="tight")
    plt.show()


def co2stop_plot():
    """
    Plot of theoretical storage sites
    """
    colors = ['gold', 'orange', 'darkgoldenrod']
    labels = ['1-3 GtCO$_2$', '3-5 GtCO$_2$', '5+ GtCO$_2$']
    ranges = [1000, 3000, 5000, 10 ** 6]
    order = 30
    n = [3, 4, 5]
    size = np.power(n, 3)

    storage_sites = gpd.read_file("raw_data/storage_sites/CO2Stop/storage_units.shp").to_crs(3035)

    ax = plot_basemap()
    fig = ax.get_figure()
    storage_sites.plot("ASSESS_UNI", ax=ax, alpha=0.5, cmap="winter")
    centroids = storage_sites.set_geometry(storage_sites.centroid)

    for i in range(0, 3):
        centroids.loc[(storage_sites['TOTAL_STOR'] >= ranges[i])
                      & (storage_sites['TOTAL_STOR'] < ranges[i + 1])].plot(
            ax=ax,
            color=colors[i],
            edgecolor="black",
            markersize=size[i],
            label=labels[i],
            zorder=order - i * 5)
        plt.legend(loc=1, prop={'size': 15})

    fig.tight_layout()
    plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\co2stop.pdf",
                bbox_inches="tight")
    plt.show()


def agglomerate_by_land_cover(variable):
    """

    :param variable: in ["land_pv", "land_wind"]
    :return:
    """
    clc = xr.open_dataset("processed_data/masks/reproj_epsg3035_500m/CLC_pv_reproj_epsg3035_500m.tif")\
        .rename({"band_data": "clc_classes"})
    land = xr.open_dataset(f"processed_data/land_availability/{variable}_epsg3035_600m.tif")\
        .rename({"band_data": variable})
    ds = xr.merge([clc, land])

    df = pd.read_excel("raw_data/tables/CLC_reclassification.xlsx")
    lc_name = dict(zip(df["value"], df["group"]))

    types = np.unique(clc["clc_classes"])
    potential_clc = {lc_name[t]: 0 for t in types}

    for t in types:
        potential_clc[lc_name[t]] += ds.where(ds["clc_classes"] == t)[variable].sum()

    #if isinstance(columns, list):
    #    data = {}
    #    for col in columns:
    #        data.update({col: [float(item[col]) for item in potential_clc.values()]})
    #    result = pd.DataFrame(data, index=list(potential_clc.keys()))

    result = pd.DataFrame({k: float(v) for k, v in potential_clc.items()}, index=[variable])

    return result

def clc_pie_chart(ds, columns):
    df1 = agglomerate_by_land_cover("land_pv")
    df2 = agglomerate_by_land_cover("land_wind")
    df = pd.concat([df1, df2])


def latitude_plot(ds, columns):
    fig = plt.figure(figsize=(12, 8))
    gs = GridSpec(3, 2, width_ratios=[1, 2], height_ratios=[1, 1, 1])
    gridlines = np.arange(2, 6) * 1e6

    names = ["Capacity Factor Wind", "Capacity Factor PV", "PV Share"]
    colors = ["royalblue", "darkorange", "limegreen"]

    ax = fig.add_subplot(gs[0, 0])

    for i, v in enumerate(columns):
        mean = ds[v].mean(dim="x")
        latitude = ds.y

        if i > 0:
            ax = fig.add_subplot(gs[i, 0], sharex=ax)

        if i < 2:
            ax.set_ylim([0, 0.55])

        ax.plot(latitude, mean, linewidth=2, color=colors[i])
        ax.set_ylabel(names[i])
        ax.xaxis.grid(True, linestyle='--', linewidth=1, color='gray', alpha=0.5)

    ax = fig.add_subplot(gs[:, 1])
    ax.set_xticks([])  # Hide x-axis ticks for the map subplot
    plot_basemap(ax=ax, yticks_right=True)

    # Add horizontal gridlines to the axes
    ax.yaxis.grid(True, linestyle='--', linewidth=1, color='gray', alpha=0.5)
    ax.set_yticks(gridlines)
    ax.tick_params(axis='y', right=True, labelright=True)

    plt.subplots_adjust(wspace=0.01, hspace=0.04)

    plt.show()


def latitude_plot_2(ds, columns):
    """

    :param ds: ds2020.data, ds2050.data
    :param columns: ["wind_CF", "pv_CF", "ratio_pv"]
    :return:
    """
    fig = plt.figure(figsize=(12, 8))
    gs = GridSpec(3, 2, width_ratios=[1, 1], height_ratios=[1, 1, 1])
    gridlines = np.arange(2, 6) * 1e6

    names = ["Capacity Factor Wind", "Capacity Factor PV", "PV Share"]
    colors = ["cornflowerblue", "darkorange", "limegreen"]

    ax = fig.add_subplot(gs[0, 0])

    for i, v in enumerate(columns):
        mean = ds[v].mean(dim="x")
        latitude = ds.y

        if i > 0:
            ax = fig.add_subplot(gs[i, 0], sharex=ax)

        if i < 2:
            ax.set_ylim([0, 0.55])

        ax.set_xlim([1.55e6, 5.25e6])
        ax.plot(latitude, mean, linewidth=2, color=colors[i])
        ax.set_ylabel(names[i])
        ax.xaxis.grid(True, linestyle='--', linewidth=1, color='gray', alpha=0.5)

    ax = fig.add_subplot(gs[2, 1])
    mean = ds["cap_dac_spec"].mean(dim="x")
    ax.plot(latitude, mean, linewidth=2, color="Red")
    ax.xaxis.grid(True, linestyle='--', linewidth=1, color='gray', alpha=0.5)
    ax.set_xlim([1.55e6, 5.25e6])
    ax.set_ylim([0.5, 1.5])
    ax2 = ax.twinx()
    ax2.set_yticks([])
    ax2.set_ylabel("DAC Capacity Ratio", rotation=-90, labelpad=45)
    ax.tick_params(axis='y', right=True, left=False, labelleft=False, labelright=True)

    ax = fig.add_subplot(gs[:2, 1])
    ax.set_xticks([])  # Hide x-axis ticks for the map subplot
    plot_basemap(ax=ax, yticks_right=True)

    # Add horizontal gridlines to the axes
    ax.yaxis.grid(True, linestyle='--', linewidth=1, color='gray', alpha=0.5)
    ax.set_yticks(gridlines)
    ax.tick_params(axis='y', right=True, labelright=True)

    plt.subplots_adjust(wspace=0.04, hspace=0.04)
    plt.show()


def pv_wind_potential_plot(ds):
    europe_overlay(ds / 625 * 1000, ["wind_potential", "pv_potential"],
                   cbar_labels=["Wind Potential [GWh/(km²a)]", "PV Potential [GWh/(km²a)]"],
                   colormaps=["Blues", "Reds"])
    plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\potential_pv_wind.pdf",
                    bbox_inches="tight")


def land_wind_pv(ds, savefig=False):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(18, 9), sharey="all")
    norm = colors.TwoSlopeNorm(vmin=0, vcenter=0.05, vmax=0.6)
    europe_map(ds, "land_pv", colormap="gist_earth_r", norm=norm,
               title="Ground-mounted PV", cbar=False, ax=ax1)
    europe_map(ds, "land_wind", colormap="gist_earth_r", norm=norm,
               title="Wind turbines", ax=ax2, cbar_label="Share of Available Land")

    plt.subplots_adjust(wspace=-0.1)

    if savefig:
        plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\land_pv_wind.pdf",
                    bbox_inches="tight")
    plt.show()


def land_pv_wind_squarified(cmap="viridis"):
    df1 = agglomerate_by_land_cover("land_pv")
    df2 = agglomerate_by_land_cover("land_wind")
    df = pd.concat([df1, df2]).T
    df.loc["Forests", "land_pv"] = 0.0001  # avoid division by zero
    df.loc["Wetland", "land_pv"] = 0.0001  # avoid division by zero
    df = df.drop("Artificial Surfaces")

    # reindex such that "Other" is at the end
    df = df.sort_values(by="land_wind", ascending=True)
    index_list = df.index.tolist()
    index_of_element = index_list.index("Other")
    index_list.append(index_list.pop(index_of_element))
    new_index = pd.Index(index_list)
    df = df.reindex(new_index)

    total_pv_land = df['land_pv'].sum()
    total_wind_land = df['land_wind'].sum()
    scale_factor = (total_pv_land + total_wind_land) / total_pv_land

    # Create a unified colormap for both treemaps
    cmap = matplotlib.cm.viridis

    x, y = 12, 6

    # Create the figure and set the size of the subplots
    fig = plt.figure(figsize=(x, y))

    # Define the gridspec layout
    a = 10
    b = x - a
    d = a * y / b / scale_factor
    c = y - d
    spec = GridSpec(ncols=2, nrows=2, width_ratios=[a, b], wspace=0.1, hspace=0.1, height_ratios=[c, d])

    # Create subplots for PV and Wind treemaps
    ax0 = fig.add_subplot(spec[:, 0])
    colors = [cmap(x) for x in np.linspace(0, 1, len(df))]
    labels_wind = [f'{(x * 100):.0f}%' for x in df['land_wind'] / total_wind_land]
    squarify.plot(df['land_wind'], label=labels_wind, alpha=0.8, color=colors, ax=ax0,
                  bar_kwargs=dict(linewidth=0.5, edgecolor="#222222"))
    ax0.text(50, 50, "Wind Turbines", color="black", va="center", ha="center")
    ax0.axis('off')

    ax1 = fig.add_subplot(spec[1, 1])
    labels_pv = [f'{(x * 100):.0f}%' for x in df['land_pv'] / total_pv_land]
    squarify.plot(sizes=df['land_pv'], label=labels_pv, alpha=0.8, color=colors, ax=ax1,
                  bar_kwargs=dict(linewidth=0.5, edgecolor="#222222"))
    ax1.text(50, 50, "PV", color="black", va="center", ha="center")
    ax1.axis('off')

    # Create the legend
    ax_legend = fig.add_subplot(spec[0, 1])
    patches = []
    for label, color in zip(df.index, colors):
        patches.append(plt.Rectangle((0, 0), 1, 1, fc=color))

    ax_legend.legend(patches, df.index, loc='center left', fontsize="small")
    ax_legend.axis('off')

    # Adjust the layout
    plt.tight_layout()

    # Show the plot
    plt.show()

    return fig, ax


def color_coded_matrix(df, ylabels=None):
    df_normalized = ((df - df.min()) / (df.max() - df.min())).T
    df = df.T

    # Set the figure size to stretch the plot
    fig, ax = plt.subplots(figsize=(len(df.columns) * 0.5, len(df.index)))

    # Plot the DataFrame using plt.imshow() with boundaries
    im = ax.imshow(df_normalized.values, cmap='RdYlGn_r', aspect='auto')

    # Add black boundaries
    for i in range(df.shape[0]):
        for j in range(df.shape[1]):
            rect = plt.Rectangle((j - 0.5, i - 0.5), 1, 1, fill=False, edgecolor='black', linewidth=1)
            ax.add_patch(rect)

    for i in range(df.shape[0]):
        for j in range(df.shape[1]):
            text = ax.text(j, i, f'{df.values[i, j]:.1f}', ha='center', va='center', color='black')

    # Set the x-axis ticks and labels
    ax.xaxis.tick_top()
    ax.set_xticks(np.arange(len(df.columns)))
    ax.set_xticklabels(df.columns)

    # Set the y-axis ticks and labels
    ax.set_yticks(np.arange(len(df.index)))
    if ylabels is None:
        ax.set_yticklabels(df.index)
    else:
        ax.set_yticklabels(ylabels)

    # Show the plot
    plt.show()


def piecharts(df):
    df = df.T

    subcategories = [
        ['DAC_mean_heat_consum', 'DAC_mean_elec_consum'],
        ['pv_footprint', 'wind_footprint', 'bat_footprint', 'hp_footprint', 'dac_footprint'],
        ['carbon_heat_cost', 'carbon_elec_cost', 'carbon_dac_cost', 'transport_cost', 'storage_cost']
    ]

    subcategory_label = [
        ['Heat', 'Electricity'],
        ['PV', 'Wind', 'Battery', 'Heat Pump', 'DAC'],
        ['Heat', 'Electricity', 'DAC', 'Transport', 'Storage']
    ]

    colors = [
        ["orangered", "cornflowerblue"],  # "dodgerblue"
        ['#FF8C00', "royalblue", "grey", "red", "dimgrey"],
        ["orangered", "cornflowerblue", "dimgrey", "limegreen", "black"]
    ]

    row_labels = ["Energy Demand", "Land Footprint", "Total Cost"]
    column_labels = ["Denmark", "Greece", "Iceland", "United Kingdom", "Switzerland"]
    categories = ["specific_energy", "specific_footprint", "specific_cost"]

    radius = np.zeros((3, 5))

    for a, cat in enumerate(categories):
        radius[a] = (df.loc[cat] / df.loc[cat].max()) ** 0.5  # quadratic

    fig, axs = plt.subplots(3, 5, figsize=(14, 8))
    threshold = 5

    def my_autopct(pct):
        if pct > threshold:
            return f'{pct:.1f}%'
        else:
            return ''

    for i, subcategory in enumerate(subcategories):
        for j, country in enumerate(df.columns):
            values = df.loc[subcategory, country].values
            axs[i, j].pie(values, autopct=my_autopct, startangle=90, pctdistance=1.2,
                          wedgeprops=dict(width=0.4),
                          radius=radius[i, j], colors=colors[i])

        legend_labels = subcategory_label[i]
        legend = axs[i, -1].legend(labels=legend_labels, loc='center', bbox_to_anchor=(1.5, 0.5), frameon=True,
                                   fontsize=12)
        legend.get_frame().set_linewidth(1)  # Set the linewidth of the legend box

    # Add row labels (vertical text on the left)
    for i, label in enumerate(row_labels):
        axs[i, 0].text(-0.3, 0.5, label, transform=axs[i, 0].transAxes,
                       rotation=90, va='center', ha='center', fontsize=14)

    # Add column labels
    for j, label in enumerate(column_labels):
        axs[0, j].text(0.5, 1.15, label, transform=axs[0, j].transAxes,
                       va='center', ha='center', fontsize=14)

    fig.tight_layout()

    plt.show()


## mask for point map:
#ds = hf.load_data("processed_data/xarray/ds_2020_10MW_median_lr_low_uptake_300_kmeans_fft").data
#mask = (ds["land_pv"] != 0)

def point_map(ds, column, label=" ", cbar=True, save_name=None, fig=None, ax=None, **kwargs):
    defaults = {
        "cmap": "Spectral_r",
        "vmin": float(ds[column].min()),
        "vmax": None,
        "legend": True,
        #"cax": None,
        "markersize": 7
    }

    d = hf.load_data("processed_data/xarray/ds_2020_10MW_median_lr_low_uptake_200_kmeans_fft").data
    mask = (d["land_pv"] != 0)

    for key, default in defaults.items():
        kwargs[key] = kwargs.pop(key, default)

    if ax is None:
        fig, ax = plt.subplots(figsize=(10, 10))

    if kwargs["vmax"] is None:
        #kwargs["vmax"] = float(np.nanquantile(ds[column], 0.98))
        pass

    if "norm" in kwargs.keys():
        kwargs["vmin"], kwargs["vmax"] = None, None

    kwargs["ax"] = kwargs.pop("ax", ax)
    fig = ax.get_figure()

    # convert to gdf
    ds[column] = ds.where(mask)[column] #ds["land_pv"] != 0 -> see above function
    gdf = gis.xarray_to_gdf(ds, [column])

    # plot basemap
    plot_basemap(color="white", boundary=False, ax=kwargs["ax"])

    # add cbar
    divider = make_axes_locatable(kwargs["ax"])
    cax = divider.append_axes("right", size="5%", pad=0.25)
    if cbar:
        kwargs["cax"] = cax
        kwargs["legend_kwds"] = {'label': label}  # "extend": "max"

    else:
        cax.axis('off')

    if kwargs.get("title") is not None:
        kwargs["ax"].set_title(kwargs.pop("title"))


    gdf.plot(column, **kwargs)
    europe.boundary.plot(ax=kwargs["ax"], alpha=0.8, edgecolor="black", linewidth=0.5)

    fig.tight_layout(rect=[0, 0, 1, 0.95])

    if save_name:
        plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\{save_name}.pdf",
                    bbox_inches="tight")

    # return fig, ax


def shared_cbar_plot(ds1, ds2, column1, column2, vcenter=500, **kwargs):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(18, 9), sharey="all")
    kwargs["norm"] = kwargs.pop("norm", colors.TwoSlopeNorm(vmin=kwargs["vmin"], vcenter=500, vmax=1500))
    point_map(ds1, column1, ax=ax1, cbar=False, **kwargs)
    point_map(ds2, column2, ax=ax2, markersize=4, label="Capturing Cost [€/tCO₂]", **kwargs)
    fig.tight_layout()
    plt.subplots_adjust(wspace=-0.05)

def latitude_wind_pv_mean():
    interval_size = 2000
    mean_wind = ds2020.data["wind_power"].mean(dim="x")
    mean_wind_smoothed = mean_wind.rolling(hourofyear=interval_size, center=False).mean()
    colors = plt.cm.viridis(np.linspace(0, 1, len(mean_wind_smoothed.y)))
    for i, y_coord in enumerate(mean_wind_smoothed.y):
        if i % 5 == 0 and i <= 100:
            x_data = mean_wind_smoothed.sel(y=y_coord)
            x_data = x_data / x_data.mean()
            plt.plot(np.arange(len(x_data)), x_data, color=colors[i], linewidth=0.5)

    plt.show()