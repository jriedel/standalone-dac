import numpy as np
import xarray as xr
import fiona
import geopandas as gpd
from geocube.api.core import make_geocube
from pathlib import Path
import pandas as pd
import os
import rasterio as rio
from rasterio.io import MemoryFile
from rasterio.features import shapes
from rasterio.mask import mask
from rasterio.transform import Affine
from rasterio.warp import reproject, Resampling, calculate_default_transform
import re
from scipy.interpolate import griddata
import shutil

import helper_functions as hf

# study area
xlim = {4326: [-25, 45], 3035: [2500000, 7500000]}
ylim = {4326: [34, 74], 3035: [1400000, 5500000]}

# time range
time_range = ['2014-01-01', '2018-12-31']

# target resolution (meter)
res = (25000, 25000)


############################## RASTER MANIPULATIONS ####################################################
def create_dataset_profile(data, profile):
    # Creates a rasterio dataset from given data and profile
    memfile = MemoryFile()
    dataset = memfile.open(**profile)
    dataset.write(data)
    return dataset


def write_raster(out_path, data, profile):
    """
    writes opened raster to .tif file

    :param out_path: str - total path (including filename.tif) where to save raster
    :param data: numpy ndarray, usually accessible via <openedraster>.read()
    :param profile: dict, usually accessible via <openedraster>.profile
    """

    with rio.open(out_path, "w", **profile) as dataset:
        dataset.write(data.squeeze(), 1)

    dataset.close()


def scale_raster(raster, xres, yres, grid=True):
    """
    :param raster: xarray dataset
    :param xres: int
    :param yres: int
    :return: xarray dataset
    """

    if isinstance(raster, (str, Path)):
        raster = xr.open_dataset(raster)

    scale_factor_x = round(abs(xres) / abs(raster.rio.resolution()[0]))
    scale_factor_y = round(abs(yres) / abs(raster.rio.resolution()[1]))

    result = raster.coarsen(x=scale_factor_x, y=scale_factor_y, boundary="pad").mean()

    if grid:
        try:
            crs = int(raster.rio.crs.data['init'].split(":")[1])
        except KeyError:
            crs = 3035
        grid_path = rf"processed_data/grid/raster_grid_{hf.string_suffix(xres, crs)}.tif"
        if not os.path.isfile(grid_path):
            create_grid(res=(xres, yres),
                        crs=crs,
                        xmin=xlim[crs][0],
                        xmax=xlim[crs][1],
                        ymin=ylim[crs][0],
                        ymax=ylim[crs][1])
        raster_grid = xr.open_dataset(grid_path)
        result = result.interp_like(raster_grid, method="nearest")

    return result


def save_multiple_resolutions(raster_path,
                              dir_out,
                              resolutions,
                              geometry,
                              include_initial_res=False):
    """
    small helper function to rescale a given raster to multiple resolutions
    and save all of them in a specified out-directory.


    :param raster_path: path to raster which should be converted - str
    :param dir_out: path to directory to store processed rasters - str
    :param resolutions: resolution tuples - list
    :param include_initial_res: True = initial resolution will also be saved - bool
    :param geometry: Shape to clip output - str/Path or geopandas GeoDataFrame
    - attributes as in rio.enums.Resampling
    """

    # define name and path
    _, raster_name = os.path.split(raster_path)
    raster = xr.open_dataset(raster_path)

    # include current resolution
    if include_initial_res:
        resolutions.append(raster.rio.resolution())

    for res_ in resolutions:
        # if resolution already stated in raster name: find and replace by regex pattern
        match_group_crs = re.search("epsg\d{4}", raster_name).group(0)
        if match_group_crs:
            name_out = re.sub("epsg\d{4}_\d+(m|deg)",
                              hf.string_suffix(res_, int(match_group_crs[-4:])),
                              raster_name)
        else:
            name_out = raster_name[:-3] + hf.string_suffix(res_) + raster_name[-3:]  # otherwise: insert it
        temp = scale_raster(raster, res_[0], res_[1])
        temp = clip_raster(temp, geometry)
        data_vars = list(temp.data_vars)
        for var in data_vars:
            temp[var].rio.to_raster(os.path.join(dir_out, name_out))


def align_2d(in_raster, match_raster, out_raster):
    if isinstance(in_raster, (Path, str)):
        in_raster = xr.open_dataset(in_raster)
    in_raster = (in_raster.squeeze()
                 .drop_vars([v for v in list(in_raster.coords) if v not in ["x", "y"]])
                 )

    if isinstance(match_raster, (Path, str)):
        match_raster = xr.open_dataset(match_raster)
    match_raster = (match_raster.squeeze()
                    .drop_vars([v for v in list(match_raster.coords) if v not in ["x", "y"]])
                    )

    aligned_raster = in_raster.interp_like(match_raster, method="nearest").rio.write_crs(3035)
    aligned_raster.rio.to_raster(out_raster)
    return aligned_raster


def align_raster(in_raster, match_raster, out_raster):
    """
    Function to align a given raster to a match raster by potentially changing the resolution,
    crs, and spatial extent of the input raster.

    :param in_raster: str/Path to input raster, which shall be aligned
    :param match_raster: str/Path to match raster (template to which in_raster shall be aligned to)
    :param out_raster: str/Path. Name and Path to location at which output raster should be saved

    modified from https://pygis.io/docs/e_raster_resample.html
    """

    # open raster to be aligned and reprojected
    with rio.open(in_raster) as src:
        # open raster to match
        with rio.open(match_raster) as match:
            dst_crs = match.crs

            # calculate the output transform matrix
            dst_transform, dst_width, dst_height = calculate_default_transform(
                src.crs,  # input CRS
                dst_crs,  # output CRS
                match.width,  # input width
                match.height,  # input height
                *match.bounds,  # unpacks input outer boundaries (left, bottom, right, top)
            )

        # set properties for output
        dst_kwargs = src.meta.copy()
        dst_kwargs.update({"crs": dst_crs,
                           "transform": dst_transform,
                           "width": dst_width,
                           "height": dst_height,
                           "nodata": src.nodata})

        # open output
        with rio.open(out_raster, "w", **dst_kwargs) as dst:
            # iterate through bands and write using reproject function
            for i in range(1, src.count + 1):
                reproject(
                    source=rio.band(src, i),
                    destination=rio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=dst_transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.nearest)


def reclassify_raster(raster_path, reclass_dictionary):
    """
    opens a raster from given path, reads the raw array, reclassifies it
    according to a given dict and returns an updated and opened rasterio dataset.
    Takes a path as positional argument (the reclass_dictionary usually takes the file
    name as key)!

    :param raster_path: path to raster - str
    :param reclass_dictionary:  dictionary to reclassify the raster values - dict
    :return: rasterio dataset
    """

    # read raster and extract raw numpy array
    if isinstance(raster_path, (str, Path)):
        raster = rio.open(raster_path)
    else:
        raster = raster_path

    data = raster.read()

    # reclassify data according to respective dict
    data = np.vectorize(reclass_dictionary.get)(data)

    # update profile from int8 to float64 (e.g. land-use factor != int)
    profile = raster.profile
    profile.update({'dtype': 'float64',
                    'nodata': -128})  # set -128 as default nodata value

    return create_dataset_profile(data, profile)


def clip_raster(raster, clip_shape, all_touched=False):
    opened_rio_raster_type = (rio.io.DatasetReader, rio.io.DatasetWriter)
    xarray_type = (xr.core.dataarray.DataArray, xr.core.dataset.Dataset)
    if isinstance(raster, opened_rio_raster_type):
        if isinstance(clip_shape, opened_rio_raster_type):
            '''
            see: 
            https://gist.github.com/cordmaur/620c6272c1fcef6274705516f28d7961#file-pfg_05_nb01-ipynb
            '''
            # get all shapes of connected regions within mask raster clip_shape
            geoms, _ = next(shapes(np.zeros_like(clip_shape.read()),
                                   transform=clip_shape.profile['transform']))

        elif isinstance(clip_shape, (gpd.geoseries.GeoSeries, gpd.GeoDataFrame)):
            geoms = clip_shape.geometry

        elif isinstance(clip_shape, (Path, str)):
            with fiona.open(clip_shape, "r") as shp:
                geoms = [feature["geometry"] for feature in shp]

        # make mask of geometries, update profile
        masked, out_transform = mask(raster, geoms)
        profile = raster.profile
        profile.update({'transform': out_transform})

        return create_dataset_profile(masked, profile)

    elif isinstance(raster, xarray_type):
        if isinstance(clip_shape, gpd.GeoDataFrame):
            geoms = clip_shape.geometry
            # see: https://corteva.github.io/rioxarray/stable/examples/clip_geom.html

        elif isinstance(clip_shape, (str, Path)):
            with fiona.open(clip_shape, "r") as shp:
                geoms = [feature["geometry"] for feature in shp]
        return raster.rio.clip(geoms, all_touched=all_touched)


def neighbors_fillna(gdf, column):
    """
    update all nans within a column of a geopandas.GeoDataFrame with the mean of its adjacent
    polygons. e.g. if column "elec_price" of Andorra was nan, it would be updated by the mean elec_price of
    Spain and France

    :param gdf: geopandas.GeoDataFrame
    :param column: str - column of interest
    :return:
    """

    nans, notnans = gdf[gdf[column].isna()].geometry, gdf[~gdf[column].isna()].geometry

    for idx in nans.index:
        distances = nans.loc[idx].distance(notnans)
        notnan_neighbors = distances[distances <= 100].index  # 100m distance =  tolerance
        gdf.loc[idx, column] = gdf.loc[notnan_neighbors, column].mean()

    return gdf

def rasterize_gdf(gdf, clip_shape, columns):
    """ create raster from geodataframe using geocube"""
    raster = make_geocube(gdf,
                          measurements=[columns],
                          output_crs=3035,
                          resolution=(-abs(res[0]), res[1]))

    grid = xr.open_dataset(rf"processed_data/grid/raster_grid_epsg3035_{abs(res[0])}m.tif")
    raster = raster.interp_like(grid, method="nearest")
    raster = raster.rio.interpolate_na(method="nearest")
    raster = clip_raster(raster, clip_shape)

    return raster




############APPLIED FUNCTIONS FOR DATA PROCESSING #####################################################
def create_grid(res,
                crs,
                xmin=xlim[3035][0],
                xmax=xlim[3035][1],
                ymin=ylim[3035][0],
                ymax=ylim[3035][1]):
    """
    :param res: tuple of x-y-resolution, e.g. (0.25, 0.25)
    :param xmin: int/float. Minimum x coordinate
    :param xmax: int/float. Maximum x coordinate
    :param ymin: int/float. Minimum y coordinate
    :param ymax: int/float. Maximum y coordinate
    :return: .tif raster file of grid, saved in folder processed_data/grid
    """

    # define number of pixel rows and columns given the resolution and spatial extent
    x_range = int((xmax - xmin) / abs(res[0]))
    y_range = int((ymax - ymin) / abs(res[1]))

    # create dummy data and transform
    data = np.zeros((y_range, x_range))
    transform = Affine.translation(xmin - abs(res[0]) / 2, ymax + abs(res[1]) / 2) * Affine.scale(res[0], - abs(res[1]))

    with rio.open(
            f'processed_data/grid/raster_grid_{hf.string_suffix(res, crs)}.tif',
            'w',
            driver='GTiff',
            height=y_range,
            width=x_range,
            count=1,
            dtype=data.dtype,
            crs={'init': f'EPSG:{crs}'},
            transform=transform,
    ) as dst:
        dst.write(data, 1)


def reproject_raster_to_grid(raster_path, out_dir, target_crs):
    """
    Function made to standardize rasters of different sources by reprojecting them to a template grid.
    The grid in target crs has an initial resolution comparable to the initial reaster.
    Once all rasters of interest are aligned, operations such as raster overlay, scaling and raster
    calculations can be performed.

    :param raster_path: str/Path to raster which will be  aligned to a standardized grid of same resolution
    :param out_dir: Directory to which aligned raster will be saved
    :return: .tif raster which contains data of input raster, but is aligned to a template grid. Rasters
    of different resolution will be saved in their resp. folder named "reproj_<res>deg" within out_dir
    """

    # open raster if only string is given
    raster = xr.open_dataset(raster_path)

    # define resolution
    try:
        src_crs = int(raster.rio.crs.data['init'].split(":")[1])
    except KeyError:
        src_crs = 3035

    # ensure that crs is written in raw_data
    raster = raster.rio.write_crs(src_crs)

    if src_crs != target_crs:
        # check if raster is sorted along x/y dim:
        for dim in ["x", "y"]:
            if not np.all(raster[dim].data[:-1] <= raster[dim].data[1:]):
                raster = raster.sortby(dim)

        raster = raster.sel(x=slice(xlim[src_crs][0], xlim[src_crs][1]),
                            y=slice(ylim[src_crs][0], ylim[src_crs][1]))

        raster = raster.rio.reproject(f"EPSG:{target_crs}")

    res_ = tuple([hf.custom_round(x, base=100) for x in raster.rio.resolution()])  # round to next 100m grid

    # define directory substring and grid to align to
    grid = f'processed_data/grid/raster_grid_{hf.string_suffix(res_, target_crs)}.tif'

    # create grid of comparable resolution in target crs if necessary
    if not os.path.isfile(grid):
        create_grid(res_,
                    target_crs,
                    xmin=xlim[target_crs][0],
                    xmax=xlim[target_crs][1],
                    ymin=ylim[target_crs][0],
                    ymax=ylim[target_crs][1])

    # define name of output raster
    _, raster_name = os.path.split(raster_path)
    raster_name = raster_name.rstrip(".tif")

    # out directory of specific resolution
    sub_dir_out = os.path.join(out_dir, f"reproj_{hf.string_suffix(res_, target_crs)}")
    if not os.path.isdir(sub_dir_out):
        os.mkdir(sub_dir_out)
    out_raster = os.path.join(sub_dir_out, raster_name + f"_reproj_{hf.string_suffix(res_, target_crs)}.tif")

    aligned_raster = align_2d(in_raster=raster,
                              match_raster=grid,
                              out_raster=out_raster)

    return aligned_raster


def raster_overlay(raster_dir,
                   out_dir,
                   overlay_name,
                   overlay_type="multiply"):
    """
    opens all rasters within a directory, sorts them by resolution, and overlays all raster with same resolution.
    Then, the overlaid rasters of different resolutions get iteratively resampled to the next lower resolution,
    and overlaid with the overlay-raster within this resolution, till the last overlay in the lowest resolution is reached.

    all masks of should have the same crs, shape, spatial extent and resolution

    :param raster_dir: str/list directory containing multiple raster/list of paths to overlay
    :param out_dir: str/path to directory in which overlay should be stored
    :param overlay_name: str  output file name, will be stored in out_dir
    :param overlay_type: "multiply" for AND overlay, works with max one non-binary mask
    """
    if isinstance(raster_dir, (Path, str)):
        raster_paths = hf.list_full_paths(raster_dir, "tif", endswith=True)
    else:
        raster_paths = raster_dir  # if list of paths
        raster_dir = os.path.split(raster_paths[0])[0]  # real directory is within path of first file

    # sort rasters by resolution:
    datasets = {}
    for path in raster_paths:
        temp = xr.open_dataset(path)
        temp_res = temp.rio.resolution()[0]
        datasets[temp_res] = datasets.get(temp_res, []) + [temp]


    for res_ in datasets:
        # define list of opened xarray datasets to overlay
        rasters = datasets[res_]

        # initialize out_data with first raster in list of rasters to overlay
        raster_0 = rasters[0]
        out_data = raster_0[list(raster_0.data_vars)[0]]  # get raw data as numpy array

        # overlay all given raster according to given method, start from second raster path
        for j in range(1, len(rasters)):
            temp_data = rasters[j][list(rasters[j].data_vars)[0]]
            if overlay_type == "multiply":
                # element-wise multiplication allows land-use factors between 0 and 1
                out_data = np.multiply(out_data, temp_data)
            elif overlay_type == "max":
                out_data = np.maximum(out_data, temp_data)

        # purge nodata issue: nodata * nodata = nodata, but here: -128 * (-128) = 16384. Set it back to -128
        out_data = xr.where(abs(out_data) >= 128, -128, out_data)

        # write data:
        out_raster = raster_0.update({list(raster_0.data_vars)[0]: out_data})
        out_raster.squeeze().rio.to_raster(os.path.join(raster_dir, f"overlay_{hf.string_suffix(res_)}.tif"))

    if len(datasets) > 1:
        resolutions = np.sort(list(datasets.keys()))  # key = res

        for j in (j for j in range(len(resolutions)-1) if len(resolutions) > 0):
            overlay_low_res = xr.open_dataset(
                os.path.join(raster_dir, f"overlay_{hf.string_suffix(resolutions[j])}.tif"))
            # get total overlay of all higher resolutions -> if 1st iteration, take the highest resolution level
            try:
                overlay_high_res = xr.open_dataset(
                    os.path.join(raster_dir, f"overlay_{hf.string_suffix(resolutions[j+1])}_tot.tif"))
            except rio.errors.RasterioIOError:
                overlay_high_res = xr.open_dataset(
                    os.path.join(raster_dir, f"overlay_{hf.string_suffix(resolutions[j + 1])}.tif"))

            # scale high-res raster to next lower res and match with lower res overlay
            overlay_resample = scale_raster(overlay_high_res,
                                            overlay_low_res.rio.resolution()[0],
                                            overlay_low_res.rio.resolution()[1])
            overlay_resample = overlay_resample.interp_like(overlay_low_res, method="nearest")

            if overlay_type == "multiply":
                data = np.multiply(overlay_resample[list(overlay_resample.data_vars)[0]],
                                   overlay_low_res[list(overlay_low_res.data_vars)[0]])
                data = xr.where(abs(data) >= 128, -128, data)
                overlay_resample.update({list(overlay_resample.data_vars)[0]: data})
            elif overlay_type == "max":
                data = np.maximum(overlay_resample[list(overlay_resample.data_vars)[0]],
                                  overlay_low_res[list(overlay_low_res.data_vars)[0]])
                data = xr.where(abs(data) >= 128, -128, data)
                overlay_resample.update({list(overlay_resample.data_vars)[0]: data})

            # save overlay with suffix _tot = total overlay of *all* levels with higher resolutions
            overlay_resample.squeeze().rio.to_raster(os.path.join(raster_dir,
                                                                  f"overlay_{hf.string_suffix(resolutions[j+1])}_tot.tif"))

        # save raster containing all overlays of higher resolutions in specified folder (here: land_availability)
        shutil.copy(os.path.join(raster_dir, f"overlay_{hf.string_suffix(resolutions[-1])}_tot.tif"),
                    os.path.join(out_dir, overlay_name))
    else:  # overlay_res_tot = overlay_res if all rasters have the same resolution
        shutil.copy(os.path.join(raster_dir, f"overlay_{hf.string_suffix(list(datasets.keys())[0])}.tif"),
                    os.path.join(out_dir, overlay_name))


####################### netCDF conversion ##############################################################
# rewritten from
def interp_to_grid(u, xc, yc, new_lats, new_lons):
    new_points = np.stack(np.meshgrid(new_lats, new_lons), axis=2).reshape((new_lats.size * new_lons.size, 2))
    z = griddata((xc, yc), u, (new_points[:, 1], new_points[:, 0]), method='nearest', fill_value=np.nan)
    return z.reshape((new_lats.size, new_lons.size), order="F")


def grid_power_timeseries(ds, resolution, x_lim=xlim[4326], y_lim=ylim[4326]):
    """
    receives xarray dataset from Europe Calliope,
    creates grid of desired resolution within specified x-y-range
    and interpolates timeseries electricity data to this grid

    https://gis.stackexchange.com/questions/455149/interpolate-irregularly-sampled-data-to-a-regular-grid
    """

    ds = ds.chunk({"site_id": -1, "time": 10})

    # define raw input data
    values = ds.electricity
    lons = ds.x
    lats = ds.y

    # Create grid on which to interpolate
    _new_x = np.linspace(x_lim[0], x_lim[1], int((x_lim[1] - x_lim[0]) / resolution[0] + 1))
    _new_y = np.linspace(y_lim[0], y_lim[1], int((y_lim[1] - y_lim[0]) / resolution[1] + 1))
    new_x = xr.DataArray(_new_x, dims="x", coords={"x": _new_x})
    new_y = xr.DataArray(_new_y, dims="y", coords={"y": _new_y})

    # Vectorize the `interp_to_grid` function.
    gridded_ds = xr.apply_ufunc(interp_to_grid,
                                values, lons, lats, new_y, new_x,
                                vectorize=True,
                                dask="parallelized",
                                input_core_dims=[['site_id'], ['site_id'], ['site_id'], ["y"], ["x"]],
                                output_core_dims=[['y', 'x']]
                                )

    return gridded_ds


def timeseries_processing(ds,
                          grid,
                          time_averaging,
                          dataset_name,
                          target_crs,
                          target_res=res,
                          reproj_res=(0.2, 0.2),  # Euro Calliope: 50km x 50km resolution ~ 0.5° x 0.5° -> safety margin
                          src_crs=4326,
                          x_lim=xlim[4326],
                          y_lim=ylim[4326],
                          t_lim=time_range):

    """
    processing of timeseries data (reproject, average, clip)
    -> used in data_processing.timeseries()
    """

    # make slices for xarray dataset slicing
    x_slice = slice(x_lim[0], x_lim[1])
    y_slice = slice(y_lim[0], y_lim[1])

    # rename dict to change coordinate names to x, y, time
    rnd = {"latitude": "y",
           "longitude": "x",
           "lat": "y",
           "lon": "x",
           "date": "time"}

    print("renaming...")
    # time slicing and renaming
    ds = ds.rename({k: rnd.get(k) for k in list(ds.variables) if rnd.get(k) is not None})  # rename variables
    print("slicing to time slice")
    t_slice = pd.date_range(t_lim[0], t_lim[1], freq=pd.to_timedelta(np.diff(ds.time)[0]), inclusive="left")
    ds = ds.sel(time=t_slice)  # ds = ds.sel(time=~ds.time.dt.is_leap_year) too slow

    # preprocess multiindex-structured wind and pv timeseries to x,y indexed xarrays
    if "site_id" in list(ds.coords):
        # select study area
        ds = ds.where((ds.x > x_lim[0]) &
                      (ds.x < x_lim[1]) &
                      (ds.y > y_lim[0]) &
                      (ds.y < y_lim[1]))
        print("interpolation to grid")
        # interpolate data to regular grid = additional step to make Calliope data accessible via x-y coordinates
        ds = grid_power_timeseries(ds, resolution=reproj_res).rename("electricity")  # 50 km resolution
        ds = ds.to_dataset(name=f"{dataset_name}_power")

    # Grouper function definition and groupby dict
    hourofyear = ((ds.time.dt.dayofyear - 1) * 24 + ds.time.dt.hour).rename('hourofyear')
    groupby_dict = {"hour": hourofyear,
                    "day": ds.time.dt.dayofyear,
                    "month": ds.time.dt.month}

    ds = ds.chunk("auto")
    print("selecting...")
    ds = ds.sel(x=x_slice, y=y_slice)  # slice to study area
    print("groupby...")
    ds = ds.groupby(groupby_dict.get(time_averaging)).mean()  # calculate time average of groups
    print("writing crs...")
    ds = ds.rio.write_crs(src_crs)
    print("reprojecting to epsg:3035...")
    ds = ds.rio.reproject(f"EPSG:{target_crs}")

    # solve nodata problem: np.nan are converted to float of order 10e+38 during re-projection
    ds[list(ds.data_vars)[0]] = xr.where(abs(ds[list(ds.data_vars)[0]]) >= 128, np.nan, ds[list(ds.data_vars)[0]])
    print("interpolating to template grid...")
    ds = ds.interp_like(grid, method="nearest")  # project data to defined grid

    # save backup
    print("saving backup...")
    ds.to_netcdf(os.path.join("processed_data/timeseries",
                              f"{dataset_name}_reproj_{hf.string_suffix(target_res, target_crs)}.nc"))

    return ds


def df_xy_to_gdf(df, resolution):
    """ create point gdf from pandas df with coordinate columns """
    if all(["x", "y"]) in df.columns:
        x = df.x
        y = df.y

    x = list(list(zip(*df.index))[df.index.names.index("x")])
    y = list(list(zip(*df.index))[df.index.names.index("y")])

    return gpd.GeoDataFrame(df, geometry=(gpd.points_from_xy(x, y)
                                          .buffer(resolution / 2, cap_style=3)))


def xarray_to_gdf(x_array, columns=None, masked=False):
    coords = np.meshgrid(x_array.x.data, x_array.y.data)
    geoms = gpd.points_from_xy(coords[0].flatten(), coords[1].flatten(), crs=3035)

    if columns:
        col_dict = {col: x_array[col].data.flatten() for col in columns}
        return gpd.GeoDataFrame(data=pd.DataFrame(col_dict), geometry=geoms)

    return gpd.GeoDataFrame(geometry=geoms)


def gdf_to_xarray(gdf, geom_col="geometry"):
    gdf.set_index([gdf[geom_col].y, gdf[geom_col].x], inplace=True)
    gdf.index.rename(["y", "x"], inplace=True)
    gdf = gdf.drop(["geometry"], axis=1)

    ds = xr.Dataset.from_dataframe(gdf)
    ds = ds.rio.write_crs(3035)

    return ds