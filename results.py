import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.colors as colors
from matplotlib.cm import ScalarMappable
import matplotlib.ticker as ticker
from matplotlib.ticker import FuncFormatter
from mpl_toolkits.axes_grid1 import make_axes_locatable
import geopandas as gpd
import seaborn as sns

import gis
import helper_functions as hf
import plotting as pt

europe = gpd.read_file(r"processed_data/maps/europe_clip_3035_wo_Turkey.shp")


def case_studies(ds):
    ds["specific_energy"] = ds["specific_energy"].where(~np.isnan(ds["LCOD"]))
    lowest_cost = ds.isel(ds["specific_cost"].argmin(dim=("y", "x")))
    smallest_land = ds.isel(ds["specific_footprint"].argmin(dim=("y", "x")))
    best_energy = ds.isel(ds["specific_energy"].argmin(dim=("y", "x")))
    uk_subset = ds.where(ds["country_idx"] == 37)
    ch_subset = ds.where(ds["country_idx"] == 26)
    uk_best = ds.isel(uk_subset["LCOD"].argmin(dim=("y", "x")))
    ch_best = ds.isel(ch_subset["LCOD"].argmin(dim=("y", "x")))

    cases = [lowest_cost, smallest_land, best_energy, uk_best, ch_best]
    abbreviations = ["DK", "EL", "IS", "UK", "CH"]
    coords = []
    keywords = ["footprint", "cost", "energy", "elec", "heat"]
    columns = [j for j in ds.data_vars if any(keyword in j for keyword in keywords)
               and "year" not in ''.join(ds[j].coords)]

    for i, item in enumerate(cases):
        temp_df = gis.xarray_to_gdf(item, columns=columns)
        temp_df.index = [abbreviations[i]]
        coords.append(temp_df)

    df = pd.concat(coords)

    return df


def country_results(ds, variable, europe, countries=None):
    country_data_var = "country_idx"

    if countries is None:
        countries = list(europe["index"])

    idx_dict = dict(zip(europe.index, europe["index"]))

    df = pd.DataFrame()

    for idx in idx_dict.keys():
        subset = ds.where(ds[country_data_var] == idx)[variable]
        df.loc[idx, f"{variable}_mean"] = subset.mean()
        df.loc[idx, f"{variable}_min"] = subset.min()
        df.loc[idx, f"{variable}_max"] = subset.max()
        df.loc[idx, f"{variable}_iqr"] = subset.mean()
        df.loc[idx, f"{variable}_sum"] = subset.sum()

    df["CNTR_CODE"] = list(idx_dict.values())

    return df


def case_study_results(ds):
    df = case_studies(ds)
    pt.color_coded_matrix(df[["specific_energy", "specific_footprint", "specific_cost"]],
                          ["Energy Demand \n[kWh/tCO₂]", "Land Footprint \n[km²/MtCO₂/a]", "Total Cost \n[€/tCO₂]"])
    pt.piecharts(df)
    ax = pt.plot_basemap()
    df.plot(marker="o", markersize=200, edgecolor="black", color="red", ax=ax)


def sorted_trends(ds, sorted_var, dependent_var, cumulative=True):
    s = ds[sorted_var].data.squeeze()
    d = ds[dependent_var].data.squeeze()
    mask = ~np.isnan(s)
    s = s[mask].flatten()
    d = d[mask].flatten()

    sorted_indices = np.argsort(s)
    sorted_s = np.sort(s)
    sorted_d = d[sorted_indices]

    if cumulative:
        cum_d = pd.Series(np.insert(sorted_d, 0, 0)).cumsum().bfill().values  # insert 0 for plotting
        sorted_s = np.insert(sorted_s, 0, sorted_s[0])
        return cum_d, sorted_s

    return sorted_d, sorted_s


def trendlines(ds, dv=None):
    if dv is None:
        dv = [item for item in ds.data_vars if 4 > len(ds[item].coords) >= 2]

    data = []

    for v in dv:
        ratio_pv, trend = sorted_trends(ds, "ratio_pv", v, False)
        data.append(trend)

    trends = pd.DataFrame(np.array(data).T, index=ratio_pv, columns=dv)

    return trends


def cost_potential_curves(ds2020, vec2020, countries, lu_mean=0.85, lu_min=False, lu_max=False, colormap="nipy_spectral"):
    """
    :param vec2020: CostCalculation object
    :param countries: list
    :param lu_min: between 0 and 1
    :param lu_mean: between 0 and 1
    :param lu_max: between 0 and 1
    :param colormap: str - plt cmap name
    :return:
    """
    ds_mean = ds2020.cdr_potential_2(lu_mean)
    ds_mean["cost"] = vec2020["specific_cost"]

    carbon_ranking = list(country_results(ds_mean, "carbon_captured", europe).sort_values("carbon_captured_sum",
                                                                                          ascending=False)["CNTR_CODE"])
    lcod_ranking = list(country_results(ds_mean, "cost", europe).sort_values("cost_min")["CNTR_CODE"])
    carbon_top_7 = carbon_ranking[:7]
    lcod_top_7 = ['DK', 'FR', 'SE', 'UK', 'LT', 'BE', 'FI', 'NL', 'IS']

    cmap = getattr(matplotlib.cm, colormap)
    colors = [cmap(x) for x in np.linspace(0, 1, len(countries))]

    fig, ax = plt.subplots(figsize=(12, 5))
    ax.set_xlabel("CDR Potential [MtCO₂/a]")
    ax.set_ylabel("Total Costs [€/tCO₂]")

    lines = []
    x_right = 0

    for i, country in enumerate(countries):
        pt.cost_quantity_plot(ds_mean, countries=[country], ax=ax, color=colors[i])
        lines.append(ax.lines[-1])

    if all([lu_min, lu_max]):
        ds_min = vec2020.cdr_potential_2(lu_min)
        ds_max = vec2020.cdr_potential_2(lu_max)
        for i, country in enumerate(countries):
            x1, y = pt.cost_quantity_plot(ds_max, countries=[country], return_instant=True)
            x2, y = pt.cost_quantity_plot(ds_min, countries=[country], return_instant=True)
            ax.fill_betweenx(y, x1, x2, color=colors[i], alpha=0.4)
            # pt.cost_quantity_plot(ds_min, countries=[country], ax=ax, color=colors[i], linestyle="--")
            # pt.cost_quantity_plot(ds_max, countries=[country], ax=ax, color=colors[i], linestyle="--")

            if np.nanmax(x1) > x_right:
                x_right = np.nanmax(x1)

    legend = ax.legend(handles=ax.lines, labels=countries)

    for line in legend.get_lines():
        line.set_linewidth(3)

    # Set gridlines
    ax.grid(True, which='major', linestyle='-', linewidth=0.5)
    ax.grid(True, which='minor', linestyle=':', linewidth=0.2)
    ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
    #ax.set_xlim([0, x_right + 50])
    fig.tight_layout()

    plt.show()


def cumulative_potential(ds2020, vec2020, lu_array=None, color="Blue"):
    max_lu = 0.85
    if lu_array is None:
        lu_array = np.linspace(0, max_lu, 11)[1:]
    x_values = []
    y_values = []
    labels = []
    for item in lu_array:
        temp_ds = ds2020.cdr_potential_2(item)
        temp_ds["cost"] = vec2020["specific_cost"]
        x, y = sorted_trends(temp_ds, "cost", "carbon_captured", cumulative=True)
        x_values.append(x/1000)  #Gt
        y_values.append(y)
        labels.append(f"{item/(max_lu/100):.0f}%")

    fig, ax = plt.subplots(figsize=(12, 6))  # figsize=(12, 5)
    cmap = "Blues"
    plt.rcParams.update({'font.size': 12})
    color_levels = np.linspace(0.2, 1.0, len(lu_array))

    for i, color_level in enumerate(color_levels):
        if len(lu_array) == 1:
            ax.plot(x_values[i], y_values[i], color="Blue", linestyle="-", linewidth=1, alpha=0.8)
        else:
            ax.plot(x_values[i], y_values[i], color=getattr(plt.cm, cmap)(color_level), linestyle="-", linewidth=1.4)
            ax.text(x_values[i][-1], y_values[i][-1], labels[i], ha='center', va='bottom')  # Add label at the last point

    ax.set_xlabel("CDR Potential [GtCO₂/a]")
    ax.set_ylabel("Total Cost [€/tCO₂]")

    # Set gridlines
    ax.grid(True, which='major', linestyle='-', linewidth=0.5)
    ax.grid(True, which='minor', linestyle=':', linewidth=0.2)
    ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
    ax.set_xlim(left=0)
    fig.tight_layout()


def cost_potential_curves_2(ds2020, vec2020, countries, colormap="nipy_spectral", lu_mean=0.85, lu_variation=0.05):
    ds = ds2020.cdr_potential_2(lu_mean)
    ds["cost"] = vec2020["specific_cost"]
    idx_dict = dict(zip(europe.index, europe["index"]))
    name_dict = dict(zip(europe.index, europe["NAME_ENGL"]))
    country_indices = [hf.get_key(idx_dict, country) for country in countries]

    #carbon_ranking = list(r.country_results(ds, "carbon_captured", europe).sort_values("carbon_captured_sum",
    #                                                                                      ascending=False)["CNTR_CODE"])
    #lcod_ranking = list(r.country_results(ds, "cost", europe).sort_values("cost_min")["CNTR_CODE"])
    #carbon_top_7 = carbon_ranking[:7]
    # [x for x in lcod_ranking[:16] if x not in carbon_ranking[:8] and x not in ["FO", "IM"]]

    fig, ax = plt.subplots(figsize=(12, 5))
    cmap = getattr(matplotlib.cm, colormap)
    def_colors = [cmap(x) for x in np.linspace(0, 1, len(countries))]

    for i, idx in enumerate(country_indices):
        subset = ds.where(ds["country_idx"] == idx)
        x, y = sorted_trends(subset, "cost", "carbon_captured", cumulative=True)
        ax.plot(x, y, label=name_dict[idx], color=def_colors[i])
        if lu_variation:
            x_max = x / lu_mean * (lu_mean + lu_variation)
            x_min = x / lu_mean * (lu_mean - lu_variation)
            ax.fill_betweenx(y, x_max, x_min, color=def_colors[i], alpha=0.2)

    ax.set_xlabel("CDR Potential [MtCO₂/a]")
    ax.set_ylabel("Total Costs [€/tCO₂]")
    legend = ax.legend()

    for line in legend.get_lines():
        line.set_linewidth(3)

    # Set gridlines
    ax.grid(True, which='major', linestyle='-', linewidth=0.5)
    ax.grid(True, which='minor', linestyle=':', linewidth=0.2)
    ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(5))
    ax.set_xlim(left=0)
    fig.tight_layout()

    # r.cost_potential_curves_2(ds2020, vec2020, carbon_ranking[:8])


def lcod_2020_2050():
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8), sharey="all")
    norm = colors.TwoSlopeNorm(vmin=0, vcenter=500, vmax=1500)
    pt.point_map(vec2020, "LCOD", ax=ax1, norm=norm, markersize=6, cbar=False, legend=False)
    pt.point_map(vec2050, "LCOD", ax=ax2, norm=norm, markersize=6, label="Capturing Cost [€/tCO₂]")
    fig.tight_layout()
    plt.subplots_adjust(wspace=-0.05)
    plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\capture_cost_2020vs2050.pdf",
                bbox_inches="tight")

def transport2020_2050():
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8), sharey="all")
    norm = colors.TwoSlopeNorm(vmin=0, vcenter=50, vmax=155)
    pt.point_map(vec2020, "transport_cost", ax=ax1, norm=norm, markersize=4, title="2020", cbar=False, legend=False)
    pt.point_map(vec2050, "transport_cost", ax=ax2, norm=norm, markersize=4, title="2050",
                 label="Transport Cost [€/tCO₂]")
    fig.tight_layout()
    plt.subplots_adjust(wspace=-0.05)


def ratios():
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8), sharey="all")
    pt.point_map(ds2020.data, "ratio_pv", ax=ax1, markersize=4, title="PV Ratio")
    pt.point_map(ds2020.data, "dac_cap_ratio", ax=ax2, markersize=4, title="DAC Capacity Ratio",
                 label="DAC Electric Nameplate Capacity per \n installed RET Capacity [kW/kW]")

def heat_bat_kde():
    hp = ds2020.data["cap_hp_spec"].data.flatten()
    hp = hp[~np.isnan(hp)]
    bat = ds2020.data["cap_bat_spec"].data.flatten()
    bat = bat[~np.isnan(bat)]

    fig, ax = plt.subplots(1, 2, figsize=(10, 3))
    sns.kdeplot(hp, ax=ax[0], color="red", fill=True)
    ax[0].set_title("Heat Pump")
    ax[0].set_xlabel("Specific Capacity [kWtₜₕ/kW]")
    ax[0].set_ylabel("Probability density")

    sns.kdeplot(bat, ax=ax[1], color="grey", fill=True)
    ax[1].set_title("Battery")
    ax[1].set_xlabel("Specific Capacity [kWh/kW]")
    ax[1].set_ylabel("Probability density")


def cf_dac_ret():
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))
    pt.point_map(vec2020 / 8760, "AEY", ax=ax1, markersize=4, title="Hybrid Renewable Power Plant")
    pt.point_map(vec2020, "dac_CF", ax=ax2, markersize=4, title="DAC Plant", label="Capacity Factor")
    fig.tight_layout()


def hybrid_pv_wind_dev():
    dev_pv = vec2020["AEY"] / 8760 - vec2020["pv_CF"]
    dev = dev_pv.squeeze().data.flatten()
    np.count_nonzero(dev[dev > 0])


def ratio_kde_20_50():
    rpv20 = ds2020.data["ratio_pv"].data.flatten()
    rpv50 = ds2050.data["ratio_pv"].data.flatten()

    rdac20 = ds2020.data["cap_dac_spec"].data.flatten()
    rdac50 = ds2050.data["cap_dac_spec"].data.flatten()

    bat20 = ds2020.data["cap_bat_spec"].data.flatten()
    bat50 = ds2050.data["cap_bat_spec"].data.flatten()

    fig, ax = plt.subplots(1, 3, figsize=(14, 3))
    sns.kdeplot(rpv20, ax=ax[0], fill=True)
    sns.kdeplot(rpv50, ax=ax[0], fill=True)
    ax[0].set_xlabel("PV Capacity Ratio")
    ax[0].set_ylabel("Probability density")

    sns.kdeplot(bat20, ax=ax[1], fill=True)
    sns.kdeplot(bat50, ax=ax[1], fill=True)
    ax[1].set_xlabel("Specific Battery Capacity [kWh/kW]")
    ax[1].set_ylabel("Probability density")

    sns.kdeplot(rdac20, ax=ax[2], fill=True, label="2020")
    sns.kdeplot(rdac50, ax=ax[2], fill=True, label="2050")
    ax[2].set_xlabel("Specific DAC Capacity [kW/kW]")
    ax[2].set_ylabel("Probability density")
    plt.legend()


def wiegner_mean(ds2020, savefig=False):
    fig, ax = plt.subplots(1, 3, figsize=(13, 4), sharey="all")
    plt.rcParams.update({'font.size': 13})
    # norm = colors.TwoSlopeNorm(vmin=0, vcenter=0.05, vmax=0.6)
    ds = ds2020.data.mean(dim="dayofyear")
    pt.europe_map(ds, "DAC_spec_elec_consum",
                  title="Electricity", ax=ax[0]) #  cbar_label="Electricity Requirements [kWh/tCO₂]"
    pt.europe_map(ds, "DAC_spec_heat_consum",
                  title="Heat", ax=ax[1]) # cbar_label="Heat Requirements [kWh/tCO₂]"
    pt.europe_map(ds, "DAC_spec_energy_consum",
                  title="Energy", ax=ax[2], cbar_label="Energy Requirements \n[kWh/tCO₂]")


    if savefig:
        plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\land_pv_wind.pdf",
                    bbox_inches="tight")
    plt.show()

def capture_storage_2020_2050():
    fig, ax = plt.subplots(2, 2, figsize=(12, 12), sharey="all")
    norm = colors.TwoSlopeNorm(vmin=0, vcenter=500, vmax=1500)
    pt.point_map(vec2020, "LCOD", ax=ax[0, 0], norm=norm, markersize=2, title="2020", cbar=False, legend=False)
    pt.point_map(vec2050, "LCOD", ax=ax[0, 1], norm=norm, markersize=2, title="2050", label="Capturing Cost [€/tCO₂]")
    fig.tight_layout()
    plt.subplots_adjust(wspace=-0.05)

    norm = colors.TwoSlopeNorm(vmin=0, vcenter=180, vmax=300)
    pt.point_map(vec2020, "transport_cost", ax=ax[1, 0], norm=norm, markersize=2, cbar=False, legend=False)
    pt.point_map(vec2050, "transport_cost", ax=ax[1, 1], norm=norm, markersize=2, label="Transport Cost [€/tCO₂]")
    fig.tight_layout()
    plt.subplots_adjust(wspace=-0.05)

    plt.savefig(rf"C:\Users\Nutzer\OneDrive - ETH Zurich\Dokumente\master\thesis\figures\capture_transport_2020vs2050.pdf",
                bbox_inches="tight")


def scatter_land():
    ratio_pv = vec2020["ratio_pv"].data.flatten()
    carbon_yield = vec2020["carbon_captured_spec"].data.flatten()
    footprint = vec2020["specific_footprint"].data.flatten()

    x = ratio_pv[~np.isnan(ratio_pv)]
    y = carbon_yield[~np.isnan(carbon_yield)]
    z = footprint[~np.isnan(footprint)]

    plt.rcParams.update({'font.size': 12})
    fig, ax = plt.subplots(figsize=(12, 5))
    scatter = ax.scatter(x, y, c=z, edgecolor="black", linewidths=0.5, s=60, cmap='YlGn')

    # Add a colorbar
    cbar = fig.colorbar(scatter, ax=ax, pad=0.03)
    cbar.set_label('Specific Footprint [km²/MtCO₂/a]')

    # Set labels and title
    ax.set_xlabel('Capacity Ratio PV')
    ax.set_ylabel('Specific Carbon Yield [tCO₂/kW]')

    fig.tight_layout()

    # Show the plot
    plt.show()

def scatter_land_energy():
    import numpy as np
    import matplotlib.pyplot as plt

    # Data and parameters
    ratio_pv = vec2020["ratio_pv"].data.flatten()
    carbon_yield = vec2020["carbon_captured_spec"].data.flatten()
    footprint = vec2020["specific_footprint"].data.flatten()
    energy = vec2020["DAC_spec_energy_consum"].mean(dim="dayofyear").data.flatten()

    x = carbon_yield[~np.isnan(carbon_yield)]
    y = footprint[~np.isnan(footprint)]
    z = ratio_pv[~np.isnan(ratio_pv)]
    size = energy[~np.isnan(energy)]

    legend_sizes = np.array([1000, 1250, 1500, 1750, 2000])
    scaled_sizes = (legend_sizes - 800) / 5

    plt.rcParams.update({'font.size': 12})
    fig, ax = plt.subplots(figsize=(12, 5))
    scatter = ax.scatter(x, y, c=z, s=(size - 800) / 5, edgecolor="black", linewidths=0.5, cmap='YlGn')

    # Add a colorbar
    cbar = fig.colorbar(scatter, ax=ax, pad=0.03)
    cbar.set_label('Capacity Ratio PV')

    # Add a legend with custom dots
    legend_dots = [ax.scatter([], [], s=s, edgecolor="black", linewidths=1, color="black") for s in scaled_sizes]
    legend_labels = [f"{str(label)} kWh/tCO₂" for label in legend_sizes]

    ax.legend(legend_dots, legend_labels, title="Specific Energy", loc="upper right")  #

    # Set labels and title
    ax.set_xlabel('Specific Carbon Yield [tCO₂/kW]')
    ax.set_ylabel('Specific Footprint [km²/MtCO₂/a]')

    fig.tight_layout()
    # Show the plot
    plt.show()

def land_ratio_pv():
    ratio_pv = vec2020["ratio_pv"].data.flatten()
    footprint = vec2020["specific_footprint"].data.flatten()
    energy = vec2020["DAC_spec_energy_consum"].mean(dim="dayofyear").data.flatten()

    x = ratio_pv[~np.isnan(footprint)]
    y = footprint[~np.isnan(footprint)]
    z = energy[~np.isnan(footprint)]

    fig, ax = plt.subplots(figsize=(12, 5))
    scatter = ax.scatter(x, y, c=z, edgecolor="black", linewidths=0.5, cmap='YlGn')

    cbar = fig.colorbar(scatter, ax=ax, pad=0.03)
    cbar.set_label("Specific Energy requirements [kWh/tCO₂]")


def cdr_distribution():
    cdr = ds2020.cdr_potential_2(0.85)
    pt.point_map(cdr / 0.625, "carbon_captured", markersize=6, label="CDR Potential [ktCO₂/km²]",
                 norm=matplotlib.colors.TwoSlopeNorm(vmin=0, vcenter=4, vmax=15))
    pt.europe_map(cdr.where(cdr["carbon_captured"] / 0.625 > 10), "carbon_captured")


def country_cdr_bar():
    cdr = ds2020.cdr_potential_2(1)
    df = results.country_results(cdr, "carbon_captured", europe)
    df = df.set_index("CNTR_CODE")["carbon_captured_sum"]
    country_data = pd.read_excel("processed_data/tables/europe_country_specific_data.xlsx").set_index("CNTR_CODE")

    df = country_data.merge(df, right_index=True, left_index=True, how="outer").sort_values("carbon_captured_sum")
    df = df[df["carbon_captured_sum"] > 10]
    df.plot(y=["carbon_captured_sum", "emissions_2"], kind="bar")

    # stacked bar
    n_levels = 10
    cmap = "Blues"
    max_level = 0.85

    # Create a stacked bar plot with progressively lighter colors
    lu_levels = np.arange(0, max_level, 0.1)
    lu_levels[-1] = max_level
    color_levels = np.linspace(0.2, 1.0, len(lu_levels))

    bottom = np.zeros(len(df))
    fig, ax = plt.subplots(figsize=(12, 3))

    for add, color_level in zip(np.diff(lu_levels), color_levels):
        ax.bar(df.index, df["carbon_captured_sum"] * add, bottom=bottom, color=getattr(plt.cm, cmap)(color_level))
        bottom += df["carbon_captured_sum"] * add

    plt.bar(df.index, df["emissions_2"], color="orange", width=0.3)
    sm = ScalarMappable(cmap=cmap, norm=plt.Normalize(0, max_level))
    sm.set_array([])

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(sm, cax=cax)
    cbar.set_label('Color', rotation=270, labelpad=5)

    # Set x and y-axis labels
    plt.xlabel('Country')
    plt.ylabel('Carbon Captured')

    # Show the plot
    plt.show()


def cdr_bar_85(ds2020):
    import matplotlib.colors as colors
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    cdr = ds2020.cdr_potential_2(0.85)
    df = country_results(cdr, "carbon_captured", europe)
    df = df.set_index("CNTR_CODE")["carbon_captured_sum"]
    country_data = pd.read_excel("processed_data/tables/europe_country_specific_data.xlsx").set_index("CNTR_CODE")

    df = country_data.merge(df, right_index=True, left_index=True, how="outer").sort_values("carbon_captured_sum")
    df = df[df["carbon_captured_sum"] > 10]

    # stacked bar
    n_levels = 10
    max_level = 1
    cmap = "Blues"
    plt.rcParams.update({'font.size': 14})

    # Create a stacked bar plot with progressively lighter colors
    lu_levels = np.arange(0, max_level + 0.1, 0.1)
    color_levels = np.linspace(0.2, 1.0, len(lu_levels))

    bottom = np.zeros(len(df))
    fig, ax = plt.subplots(figsize=(12, 5))

    for add, color_level in zip(np.diff(lu_levels), color_levels):
        ax.bar(df.index, df["carbon_captured_sum"] * add, bottom=bottom, color=getattr(plt.cm, cmap)(color_level))
        ax.tick_params(axis='both', which='major', labelsize=12)  # Adjust the font size of tick labels along the axes
        bottom += df["carbon_captured_sum"] * add

    plt.bar(df.index, df["emissions_2"], color="orange", width=0.3, label="CO₂ Emissions 2020")

    # Create a custom color map with discrete colors
    colormap = colors.ListedColormap(getattr(plt.cm, cmap)(color_levels))

    # Create a colorbar with custom tickmarks
    bounds = np.linspace(0, max_level, len(color_levels))
    norm = colors.BoundaryNorm(bounds, colormap.N)

    ax.set_ylabel("Carbon Mass Flux [MtCO₂/a]")
    ax.set_xlabel("Countries")
    ax.legend()

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="2%", pad=0.1)

    # Create a mappable object using ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=norm)
    sm.set_array([])  # Set an empty array

    cbar = plt.colorbar(sm, cax=cax)
    cbar.set_label('DACCS Potential Exploitation', rotation=270, labelpad=5)

    # Format ticklabels as percentages
    cbar.ax.yaxis.set_major_formatter(FuncFormatter(lambda x, _: '{:.0%}'.format(x)))

    fig.tight_layout()

    plt.show()


def graph_plot(access_nodes):
    total_network = hf.load_data("processed_data/transport/total_network.shp")
    storage_sites = gpd.read_file("processed_data/transport/Northern_lights.shp").to_crs(3035)
    ax = pt.plot_basemap()
    fig = ax.get_figure()

    total_network["color"] = "cornflowerblue"
    total_network.loc[total_network["mode"] == "rail", "color"] = "forestgreen"
    total_network["linestyle"] = "solid"
    total_network.loc[total_network["mode"] == "rail", "linestyle"] = "dashed"

    total_network.plot(ax=ax,
                       color=total_network["color"],
                       linewidth=1,
                       zorder=0)
    access_nodes.plot(ax=ax, markersize=10, color="white", edgecolor="black", zorder=1)
    storage_sites.plot(ax=ax, marker="s", markersize=80, color="Red", edgecolor="black", zorder=2)

    fig.tight_layout()
    plt.show()

def dac_CF_bat_kde():
    hp = ds2020.data["dac_CF"].data.flatten()
    hp = hp[~np.isnan(hp)]
    bat = ds2020.data["cap_bat_spec"].data.flatten()
    bat = bat[~np.isnan(bat)]

    fig, ax = plt.subplots(1, 2, figsize=(6, 6))
    sns.kdeplot(hp, ax=ax[0], color="black", fill=True)
    ax[0].set_xlabel("DAC capacity factor")
    ax[0].set_ylabel("Probability density")

    sns.kdeplot(bat, ax=ax[1], color="darkgreen", fill=True)
    ax[1].set_xlabel("Specific battery capacity \n [kWh/kW]")
    ax[1].set_ylabel("")
    fig.tight_layout()

