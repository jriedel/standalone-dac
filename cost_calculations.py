import geopandas as gpd
import numpy as np
from numba import njit
import pandas as pd
from scipy.optimize import fsolve
import xarray as xr

import helper_functions as hf
import classification as cfc
import gis
import plotting as pt


class CostCalculation:
    def __init__(self, xr_dataset, study_area, year, cap_ret, lr, tech_uptake):
        self.year = year
        self.cap_ret = cap_ret
        self.lr = lr
        self.uptake = tech_uptake
        self.wacc = 0.061  # mean WACC of all pixels regional WACC applied later
        self.lifetime = 25
        self.data = xr_dataset
        self.params, self.units = self.get_params()
        self.cell_size = abs(self.data.rio.resolution()[0] * self.data.rio.resolution()[1]) / 1e6  # km²
        self.study_area = study_area

    def get_params(self, year=None):
        # params of all technologies apart from DAC
        params = pd.read_excel("processed_data/tables/params.xlsx",
                               skiprows=2,
                               usecols="A:F",
                               index_col=0).dropna(how="all")
        if not year:
            year = self.year

        params_dict = params[year].to_dict()
        units_dict = params["unit"].to_dict()

        # DAC conversion factor -> investment given as function CAPEX(capacity_DAC[tCO2]).
        # Substitute capacity_DAC[tCO2] by capacity_DAC[kW] * conversion_factor
        coordinates_climeworks = (4.233e6, 2.688e6)
        climeworks = self.data.sel(x=coordinates_climeworks[0], y=coordinates_climeworks[1], method="nearest")

        # power demand of climeworks Hinwil plant [kW_installed] -> captures 960 tCO2/y -> what is nameplate power?
        def conversion(power_cap_dac):
            return (0.9 * power_cap_dac / (climeworks.DAC_spec_energy_consum.data.repeat(24)
                                           + params_dict["dac_compression_work"])).sum() - 960

        # solve for nameplate capacity -> conversion factor to translate CAPEX(tCO2/y) to CAPEX(kW DAC capacity)
        conversion_factor = fsolve(conversion, np.array(3300))[0] / 960  # kW installed per tCO2 removed annually

        # return all parameters in one dict
        params_dict.update({"dac_conversion_factor": float(conversion_factor)})
        units_dict.update({"dac_conversion_factor": "kW dac capacity / tCO2 removed annually"})

        return params_dict, units_dict

    def get_params_dac(self, dac_power, update=False):
        """
               :param dac_power: float - nameplate power consumption of DAC in kW
               :param update: Whether the parameters in the CostCalculation class should be updated or not
               :return: scaled Capex and Opex in € for a DAC plant with specified capacity, carbon_capacity = carbon
               removal potential in tCO2 if plant was placed in Hinwil and ran on 90% capacity factor
        """

        # FOAK estimates
        foak_scale = 960
        foak_capex_young = (1.31 + 0.768 + 0.115 + 0.748 + 0.00230 + 0.278 + 1.37 + 0.105 + 0.191 + 1.69) * 1000000

        # Uptake scenarios
        lr_dict = dict(zip(["min", "median", "max"], [0.1, 0.15, 0.18]))
        uptake_scenarios = pd.DataFrame(dict(zip(["low", "high"], [[foak_scale, 2e6, 10e6, 500e6, 1800e6],
                                                                   [foak_scale, 180e6, 11900e6, 30200e6, 31600e6]])),
                                        index=[2020, 2030, 2050, 2075, 2100])  # tCO2, taken from ESM of Young et al.

        def learn_exp(lr):
            return -np.log(1 - lr) / np.log(2)

        # future capex @ FOAK scale, but with technology learning (top-down approach)
        capex_prediction = foak_capex_young * (uptake_scenarios.loc[self.year, self.uptake] / foak_scale) ** (-learn_exp(lr_dict[self.lr]))

        # corresponding carbon removal capacity (if the plant was built in Hinwil)
        carbon_capacity = dac_power / self.params["dac_conversion_factor"]  # tCO2/y

        # scaling ratio between FOAK DAC and DAC of interest
        ratio = carbon_capacity / foak_scale

        # CAPEX [€] of DAC plant with annual capturing capacity of x tCO2 (in Hinwil!)
        scaled_capex = capex_prediction * ratio ** 0.91

        if update:
            self.params.update({"dac_capex": scaled_capex})

        return scaled_capex

    def rasterize_country_data(self, columns):
        """
        Country-specific data is already stored as columns in GeoDataFrame study_area. To make it accessible
        as xarray datasets/rasters in self.data, selected columns get rasterized via gis.rasterize_gdf
        :param columns: list - all columns to add to self.data
        """

        if isinstance(columns, str):
            columns = [columns]

        for col in columns:
            if col in ["NAME_ENGL", "country", "index"]:
                self.study_area[f"{col}_idx"] = self.study_area.index
                rasterized = gis.rasterize_gdf(self.study_area, self.study_area, f"{col}_idx")
            else:
                gis.neighbors_fillna(self.study_area, col)
                rasterized = gis.rasterize_gdf(self.study_area, self.study_area, col)
            self.data = self.data.merge(rasterized)

        return rasterized

    def classify(self, variables, method, n_clusters=None, normalization=None, transformation=None):
        clustered_ds, clustered_data = cfc.make_cluster(self.data,
                                                        variables,
                                                        n_clusters=n_clusters,
                                                        method=method,
                                                        normalization=normalization,
                                                        transformation=transformation)

        self.data = self.data.merge(clustered_ds)

    def calculate_ret_potential(self):
        if np.isnan(self.params["wind_power_density"]):
            self.params.update({"wind_power_density": 14.81})  # McKenna et al. Vestas V90

        for technology in ["pv", "wind"]:
            self.data[f"{technology}_power_potential"] = (self.data[f"land_{technology}"]
                                                          * self.cell_size
                                                          * self.params[f"{technology}_power_density"] * 1000  # kW
                                                          )
            self.data[f"{technology}_potential"] = self.data[f"{technology}_power_potential"] \
                                                   * self.data[f"{technology}_CF"] * 8760 / 1e9  # TWh/year

            self.data[f"{technology}_power_potential"].attrs["unit"] = "kW"
            self.data[f"{technology}_potential"].attrs["unit"] = "TWh/y"

    def create_example_point(self):
        x_min, y_min, x_max, y_max = self.study_area.total_bounds

        x = np.random.uniform(x_min, x_max, 100)
        y = np.random.uniform(y_min, y_max, 100)

        gdf = gpd.GeoDataFrame({"x": x, "y": y}, geometry=gpd.points_from_xy(x, y)).set_crs(3035)
        points = gpd.sjoin(self.study_area, gdf)
        sample = points.sample()

        point = self.data.sel(x=float(sample.x), y=float(sample.y), method="nearest")
        point["country"] = sample.NAME_ENGL
        return point

    def calculate_lcoe(self):
        lcoe_vector = np.vectorize(lcoe)
        for technology in ["pv", "wind"]:
            result = lcoe_vector(self.params[f"{technology}_capex"],
                                 self.data[f"{technology}_CF"],
                                 self.data["WACC"],
                                 self.params[f"{technology}_lifetime"],
                                 *[self.params[key] for key in self.params.keys() if f"{technology}_opex" in key])
            self.data = self.data.assign({f"{technology}_lcoe": (["y", "x"], result)})

    def cheap_tech_mask(self, technology):
        dic = {"pv": "wind", "wind": "pv"}
        mask = xr.where(self.data[f"{technology}_lcoe"] <= self.data[f"{dic[technology]}_lcoe"], 1, np.nan)
        return mask

    def lcod(self, ratio_pv, dac_cap_ratio, point):
        """
        calculation of the levelized cost of a captured ton of carbon at a certain location
        based on the chosen input parameters.

        :param point: subset of self.data DataSet, created by self.data.sel(x=... y=...) -> point in space
        :param ratio_pv: given ratio of PV
        :param dac_cap_ratio: ratio between DAC nameplate electric power and nameplate RET generation.
        :return: float - levelized cost of DAC
        """

        # weighted overlay of pv and wind timeseries, output in kW
        e_out = self.cap_ret * (point.pv_power * ratio_pv + point.wind_power * (1 - ratio_pv))

        # ensure that dac capacity is always >= mean generation
        if e_out.data.mean() > dac_cap_ratio * self.cap_ret:
            dac_cap_ratio = e_out.data.mean() / self.cap_ret

        # smooth e_out to e.g. 0.7 * nameplate power by battery of capacity cap_bat
        e_out_raw = e_out.copy()
        e_out, cap_bat = battery_smoothing(e_out.data,
                                           dac_cap_ratio,
                                           self.cap_ret,
                                           self.params["bat_charg_eff"],
                                           self.params["bat_discharg_eff"])
        bat_power = e_out - e_out_raw
        bat_out = np.sum(bat_power[bat_power > 0])

        # share of electricity consumed by HP at each timestep
        ratio_hp = (point.DAC_spec_heat_consum.data.repeat(24) / self.params["hp_cop"] /
                    (point.DAC_spec_heat_consum.data.repeat(24) / self.params["hp_cop"]
                     + point.DAC_spec_elec_consum.data.repeat(24)
                     + self.params["dac_compression_work"])
                    )

        # absolute electricity consumption of heat pump and DAC plant
        hp_consum = e_out * ratio_hp  # electricity consumed by HP to provide heat for DAC
        elec_consum = e_out * (1 - ratio_hp)  # electricity consumed directly by DAC

        # if DAC ran as baseload
        hp_consum_base, elec_consum_base = np.max(e_out) * np.array([ratio_hp, (1 - ratio_hp)])
        dac_power_base = elec_consum_base + hp_consum_base * self.params["hp_cop"]

        # heat variables
        heat_out = hp_consum * self.params["hp_cop"]  # heat flow out of HP [kWhth / h]
        heat_out_max = hp_consum_base * self.params["hp_cop"]  # same if energy was always available
        cap_hp = heat_out.max()  # thermal nameplate capacity of heat pump [kWth]
        hp_cf = heat_out.sum() / heat_out_max.sum()  # capacity factor of heat pump

        # DAC nameplate power (based on heat and electricity consumption, not electric load (energy consumption w/o HP)
        dac_power = (elec_consum + heat_out)
        cap_dac = dac_power.max()

        # find out carbon captured timeseries
        carbon_captured = dac_power / (point.DAC_spec_energy_consum.data.repeat(24)
                                       + self.params["dac_compression_work"])
        max_carbon = dac_power_base / (point.DAC_spec_energy_consum.data.repeat(24)
                                       + self.params["dac_compression_work"])
        dac_CF = carbon_captured.sum() / max_carbon.sum()

        # LCOE components
        pv_annual_costs = self.cap_ret * ratio_pv * (self.params["pv_capex"] * crf(self.wacc, self.params["pv_lifetime"])
                                                     + self.params["pv_opex"]
                                                     )
        wind_annual_costs = self.cap_ret * (1 - ratio_pv) * (self.params["wind_capex"] * crf(self.wacc, self.params["wind_lifetime"])
                                                             + self.params["wind_opex_var"] * point.wind_CF
                                                             + self.params["wind_opex_fix"]
                                                             )
        bat_annual_costs = cap_bat * (self.params["bat_capex"] * crf(self.wacc, self.params["bat_lifetime"])
                                      + self.params["bat_opex_fix"]) + bat_out * self.params["bat_opex_var"]

        LCOE = (pv_annual_costs + wind_annual_costs + bat_annual_costs) / e_out.sum()

        # cost of heating system
        hp_annual_costs = cap_hp * (self.params["hp_capex"] * crf(self.wacc, self.params["hp_lifetime"])
                                    + self.params["hp_opex_var"] * heat_out.sum() / cap_hp.data  # CF HP
                                    + self.params["hp_opex_fix"]
                                    )

        LCOH = hp_annual_costs / heat_out.sum() + LCOE / self.params["hp_cop"]

        # levelized cost of capturing a tonne of CO2
        dac_capex = self.get_params_dac(cap_dac, update=False) * crf(self.wacc, self.lifetime)
        dac_opex = dac_capex * 0.04 + self.params["dac_sorbent_resupply"] * carbon_captured.sum()

        LCOD = ((dac_capex + dac_opex) / carbon_captured.sum()  # already annualized above
                + LCOE * point.DAC_spec_elec_consum.data.mean()
                + LCOH * point.DAC_spec_heat_consum.data.mean()
                )

        return np.array(LCOD), ratio_pv, dac_cap_ratio, cap_dac / self.cap_ret, dac_CF, \
            carbon_captured.sum() / self.cap_ret, cap_bat / self.cap_ret, cap_hp / self.cap_ret, hp_cf, \
            e_out.sum() / e_out_raw.sum(), bat_out / self.cap_ret

    def lcod_pso(self, point, particles, iterations, animate=False):
        """
        Calculate the lowest LCOD at a point by approximating the best (a) ratio between wind
        and PV plant capacities and (b) the best ratio of DAC nameplate power and maximum power
        generation. The approximation is done via Particle Swarm Optimization (pso) defined by the
        `pso()` function outside the class.

        :param point: xarray DataSet - subset of self.data, selected by e.g. self.data.sel(x=..., y=...)
        :param particles: int -  numbers of swarm particles
        :param iterations: int - self-explanatory
        :return: list - return of the function self.lcod evaluated at the optimal point
        """

        range_ratio_pv = (0, 1)
        range_dac_cap_ratio = (0.1, 1)  # 0 has to be excluded (feasibility), ~0.13 is min(dac_cap_ratio) for pure PV
        lowest_lcod, position, data = pso(self.lcod,
                                          range_ratio_pv,
                                          range_dac_cap_ratio,
                                          particles,
                                          iterations,
                                          function_args=[point])
        if animate:
            pt.animate_scatter_xy(data,
                                  range_ratio_pv,
                                  range_dac_cap_ratio)

        result = self.lcod(position[0], position[1], point)

        return result

    def energy_system_mapper(self, class_group, particles=50, iterations=50):
        """
        Function to calculate the optimal solution for all clusters by (i) selecting a representative point within each
        cluster [see: select_representative_observation function], (ii) solving the PSO for these representative points,
        (iii) broadcasting the outcomes to all pixels within the class

        NOTE that these results only pertain to the wind/pv/temperature/humidity timeseries of the single
        representative point and constant WACC -> are therefore assumed valid for all points within each cluster.
        For improving these caveats, i.e. by simulating the energy system results with the raw timeseries data at
        all points with regional WACC see method `vectorized_lcod`.

        :param class_group: str - name of cluster group to group-by (format: 'classes_{method}_{n_clusters}_{transformation}')
        :param particles: int - No. of particles used in PSO
        :param iterations: int - max iterations in the PSO
        :return:
        """

        def select_representative_observation(group):
            variables = ["pv_power", "wind_power", "tg", "hu"]
            subset = group[variables]
            mean = subset.mean(dim=["hourofyear", "dayofyear"])
            diff = subset - mean
            std = diff.std(dim=["hourofyear", "dayofyear"])
            total_std = std["wind_power"] + std["pv_power"] + std["tg"] + std["hu"]
            representative_idx = total_std.argmin(dim="site")
            return group.isel(site=representative_idx)

        groups = (self.data
                  .stack(site=("y", "x"))
                  .groupby(class_group)
                  .apply(select_representative_observation)  # get point with smallest std in each class
                  )

        classes = []
        results = []
        lcod_return = ["LCOD", "ratio_pv", "dac_cap_ratio", "cap_dac_spec", "dac_CF", "carbon_captured_spec",
                       "cap_bat_spec", "cap_hp_spec", "hp_CF", "ret_eff", "bat_out"]

        lcod_return_units = ["€/tCO2", "", "dac_cap/ret_cap", "dac_nameplate_cap/cap_ret", "", "tCO2/y/kW cap_ret",
                             "kWh_storage/kW ret_cap", "kWth/kW ret_cap", "", "kWh delivered/kWh produced",
                             "kWh_bat_output/kW ret_cap"]

        self.units.update(dict(zip(lcod_return, lcod_return_units)))

        flag = 0
        number_of_classes = len(groups[class_group].data)
        for class_ in groups[class_group].data:  # iterate through all classes/clusters
            flag += 1
            print(f"Calculating optimal energy system for class {flag} of {number_of_classes}...")
            classes.append(class_)
            point = groups.sel({class_group: class_})  # select the random point within class_
            point_results = self.lcod_pso(point=point,
                                          particles=particles,
                                          iterations=iterations
                                          )

            results.append(dict(zip(lcod_return, point_results)))

        df = pd.DataFrame(results)
        df.insert(0, class_group, np.array(classes))

        # Broadcast the PSO results to all pixels in the self.data DataArray according to their resp. cluster/class
        for col_name in lcod_return:
            classes_dict = dict(zip(df[class_group], df[col_name]))
            self.data = (self.data.assign(
                {col_name: (["y", "x"], np.vectorize(classes_dict.get)(self.data[class_group].data))})
                         .astype(np.float32)
                         )
            self.data[col_name].attrs["unit"] = self.units.get(col_name)
        self.data = self.data.squeeze()  # remove unnecessary dims
        return df

    def vectorized_lcod(self):
        """
        Calculates the LCOD and all other cost contributions for a system connected
        to self.cap_ret kW installed RET capacity
        """

        if "WACC" not in self.data.data_vars:
            self.rasterize_country_data("WACC")

        result = self.data.copy()

        print("Define relevant variables")
        result["cap_pv_spec"] = result["ratio_pv"]
        result["cap_wind_spec"] = 1 - result["ratio_pv"]
        result["DAC_mean_heat_consum"] = result["DAC_spec_heat_consum"].mean(dim="dayofyear")
        result["DAC_mean_elec_consum"] = result["DAC_spec_elec_consum"].mean(dim="dayofyear") \
                                          + self.params["dac_compression_work"]

        result["e_out"] = (result["pv_power"] * result["ratio_pv"]
                            + result["wind_power"] * (1 - result["ratio_pv"])) * self.cap_ret

        # electricity cost
        print("Calculate LCOE")
        annual_elec_cost = np.sum([annualized_costs(self.params[f"{tech}_capex"],
                                                    result[f"{tech}_CF"],
                                                    result["WACC"],
                                                    self.params[f"{tech}_lifetime"],
                                                    * hf.get_key_from_string(self.params, f"{tech}_opex"))
                                   * result[f"cap_{tech}_spec"] * self.cap_ret for tech in ["pv", "wind"]]
                                  + [((self.params["bat_capex"] * crf(result["WACC"], self.params["bat_lifetime"])
                                       + self.params["bat_opex_fix"]) * result[f"cap_bat_spec"]
                                      + result["bat_out"] * self.params["bat_opex_var"]) * self.cap_ret], axis=0)

        LCOE = annual_elec_cost / (result["e_out"] * result["ret_eff"]).sum(dim="hourofyear")
        result["AEY"] = (result["e_out"] * result["ret_eff"]).sum(dim="hourofyear", skipna=False) / self.cap_ret

        # heat costs
        print("Calculate LCOH")
        annual_heat_cost = annualized_costs(self.params["hp_capex"], result["hp_CF"], result["WACC"],
                                            self.params["hp_lifetime"], *hf.get_key_from_string(self.params, "hp_opex"))
        LCOH = annual_heat_cost / (result["cap_hp_spec"] * result["hp_CF"] * 8760) + LCOE / self.params["hp_cop"]

        # DAC costs
        print("Calculate LCOD")
        cap_dac = self.cap_ret * result["cap_dac_spec"]  # absolute values
        carbon_captured = cap_dac * result["dac_CF"] * 8760 / (result["DAC_mean_elec_consum"]
                                                               + result["DAC_mean_heat_consum"])

        dac_capex = self.get_params_dac(cap_dac, update=False) * crf(result["WACC"], self.lifetime)
        dac_opex = dac_capex * 0.04 + self.params["dac_sorbent_resupply"] * carbon_captured
        annual_dac_cost = (dac_capex + dac_opex)  # absolute values

        result["specific_elec"] = (result["AEY"] * self.cap_ret) / carbon_captured
        result["specific_energy"] = result["DAC_mean_elec_consum"] + result["DAC_mean_heat_consum"]
        result["specific_compression"] = self.params["dac_compression_work"]
        result["carbon_heat_cost"] = LCOH * result["DAC_mean_heat_consum"]
        result["carbon_elec_cost"] = LCOE * result["DAC_mean_elec_consum"]
        result["carbon_dac_cost"] = annual_dac_cost / carbon_captured

        LCOD = (result["carbon_dac_cost"]
                + result["carbon_elec_cost"]
                + result["carbon_heat_cost"])

        result["LCOE"], result["LCOH"], result["LCOD"] = LCOE, LCOH, LCOD

        subset = [item for item in result.data_vars if "carbon_" in item]
        for i in subset:
            result[i].attrs["unit"] = "€/tCO2"

        return result

    def carbon_lcod(self, carbon_captured):
        """
        Function to scale specific values of energy system components to absolute values required
        for capturing a specified amount of CO2 annually. All relevant cost components, LCOE, LCOH,
        and LCOD get returned as a new xarray Dataset.
        """

        # check if power potential is already calculated
        if "pv_power_potential" not in self.data.data_vars:
            self.calculate_ret_potential()

        cap_ret_scaling = carbon_captured / self.data.carbon_captured_spec

        return self.scale_energy_system(cap_ret_scaling, carbon_captured)

    def cdr_potential(self):
        """
        defines the CDR potential of all pixels / DAC locations by scaling the standalone DAC system linearly to such
        size that all free land is occupied
        :return:
        """
        # check if power potential is already calculated
        if "pv_power_potential" not in self.data.data_vars:
            self.calculate_ret_potential()

        elif "country_idx" not in self.data.data_vars:
            self.rasterize_country_data("country")

        # find absolute values of energy systems
        cap_ret_scaling = np.minimum(self.data["pv_power_potential"] / self.data["ratio_pv"],
                                     self.data["wind_power_potential"] / (1 - self.data["ratio_pv"]))
        carbon_captured = cap_ret_scaling * self.data["carbon_captured_spec"]

        return self.scale_energy_system(cap_ret_scaling, carbon_captured)

    def cdr_potential_2(self, LU_factor=None):
        """
        same as method `cdr_potential`, but includes a land-use factor between 0 (no exploitation)
        and 1 (full exploitation)

        :param LU_factor:
        :return:
        """

        self.system_footprint(scale=self.cap_ret)  # yields absolute values of footprints
        self.calculate_ret_potential()
        self.rasterize_country_data(["country"])

        self.params["pv_land_share"] = LU_factor
        self.params["wind_land_share"] = LU_factor

        # Scaling parameter ->  how many of the systems with size self.cap_ret can be built?
        cap_ret_scaling = np.minimum(self.params["pv_land_share"] * self.data["land_pv"]
                                     * self.cell_size / self.data["system_footprint"],
                                     self.params["wind_land_share"] * (self.data["land_tot"] - self.data["land_pv"])
                                     * self.cell_size / self.data["wind_footprint"])

        return self.scale_energy_system_2(cap_ret_scaling)

    def system_footprint(self, scale=False):
        """
        units: all data_vars with "footprint" are in km²/scale, e.g. km²/system connected to 10MW RET capacity
        only the variable "specific footprint" is in km²/MtCO2 removed annually
        :param scale:
        :return:
        """
        if not scale:
            scale = self.cap_ret  # generically absolute footprint of default size
        self.data["pv_footprint"] = self.data["ratio_pv"] * scale / self.params["pv_power_density"] / 1000  # km2
        self.data["wind_footprint"] = (1-self.data["ratio_pv"]) * scale / self.params["wind_power_density"] / 1000
        self.data["bat_footprint"] = self.data["cap_bat_spec"] * scale / self.params["bat_power_density"] / 1000
        self.data["hp_footprint"] = self.data["cap_hp_spec"] * scale / self.params["hp_power_density"] / 1000

        # calculate DAC footprint by using parameters of data based on Mt instead of MW capacity
        carbon_capacity = self.data["cap_dac_spec"] * scale / self.params["dac_conversion_factor"]  # tCO2
        self.data["dac_footprint"] = carbon_capacity / self.params["dac_power_density"] / 1000000  # km2

        # system has same constraints as PV
        self.data["system_footprint"] = self.data["hp_footprint"] + self.data["bat_footprint"] \
                                        + self.data["pv_footprint"] + self.data["dac_footprint"]
        self.data["total_footprint"] = self.data["system_footprint"] + self.data["wind_footprint"]
        self.data["specific_footprint"] = (self.data["total_footprint"] / scale) / self.data["carbon_captured_spec"] * 1e6


    def scale_energy_system(self, cap_ret, carbon_captured):
        """
        Upscaling of standalone DAC systems. Here economics of scale are applied, i.e. for areas of vast land ressources
        one gigantic single standalone DAC system with lower specific costs is assumed.

        :param cap_ret: Total installed nameplate capacity of RETs (PV + wind)
        :param carbon_captured: xr.DataArray with pixels-wise absolute amount of carbon captured annually
        :return: xr DataSet with scaled absolute sizes of standalone DAC systems + their costs
        """

        # linear scaling of system size. scale_energy_system_2 assume multiple systems of same size installed
        result = xr.Dataset()
        result["country_idx"] = self.data["country_idx"]
        result["carbon_captured"] = carbon_captured
        result["cap_ret"] = cap_ret
        result["ratio_pv"] = self.data["ratio_pv"]
        result["dac_CF"] = self.data["dac_CF"]
        result["dac_cap_ratio"] = self.data["dac_cap_ratio"]

        # find absolute values of energy system
        result["cap_pv"] = cap_ret * self.data.ratio_pv
        result["cap_wind"] = cap_ret * (1 - self.data.ratio_pv)

        for item in ["cap_hp", "cap_bat", "cap_dac"]:
            result[item] = self.data[f"{item}_spec"] * cap_ret

        # annual electricity and heat generation
        # explanation: dac_cap_ratio * cap_ret = max(e_out_smoothed) = "cap_dac" (without HP!)
        e_out_sum = (self.data["dac_cap_ratio"] * cap_ret * self.data["dac_CF"]) * 8760
        heat_out_sum = (result["cap_hp"] * self.data["hp_CF"]) * 8760

        # annual technology costs
        for tech in ["pv", "bat"]:
            result[f"{tech}_annual_costs"] = result[f"cap_{tech}"] * (
                self.params[f"{tech}_capex"] * crf(self.data["WACC"], self.params[f"{tech}_lifetime"])
                + self.params[f"{tech}_opex"])

        for tech in ["wind", "hp"]:
            result[f"{tech}_annual_costs"] = result[f"cap_{tech}"] * (
                self.params[f"{tech}_capex"] * crf(self.data["WACC"], self.params[f"{tech}_lifetime"])
                + self.params[f"{tech}_opex_var"] * self.data[f"{tech}_CF"]
                + self.params[f"{tech}_opex_fix"])

        # cost of dac
        dac_capex = self.get_params_dac(result["cap_dac"]) * crf(self.data["WACC"], self.lifetime)
        dac_opex = dac_capex * 0.04 + self.params["dac_sorbent_resupply"] * carbon_captured
        result["dac_annual_costs"] = dac_capex + dac_opex

        # electricity and heat requirements (weighted mean)
        result["dac_spec_elec_consum"] = (e_out_sum - heat_out_sum / self.params["hp_cop"]) / carbon_captured
        result["dac_spec_heat_consum"] = heat_out_sum / carbon_captured

        result["LCOE"] = (result["pv_annual_costs"] + result["wind_annual_costs"] + result["bat_annual_costs"]) / e_out_sum
        result["LCOH"] = result["hp_annual_costs"] / heat_out_sum + result["LCOE"] / self.params["hp_cop"]
        result["LCOD"] = (result["dac_annual_costs"] / carbon_captured
                          + result["LCOE"] * result["dac_spec_elec_consum"]
                          + result["LCOH"] * result["dac_spec_heat_consum"]
                          )

        return result

    def scale_energy_system_2(self, scale):  # linear scaling
        """
        Scaling of DAC systems according to the notion that not single systems get scaled linearly, but rather
        multiple systems get installed instead of single ones -> no economics of scale. Reasoning: Land is fragmented,
        in cells with 625km² size and LU of e.g., 0.4, one cannot assume that there is a single contigous area of
        250km², but rather multiple smaller areas with - potentially - multiple DAC systems

        :param scale: float - scale factor
        :return: spatial distribution of carbon captured, LCOD and number of systems
        """
        result = xr.Dataset()
        result["carbon_captured"] = self.data["carbon_captured_spec"] * scale * self.cap_ret / 1e6  # Mt
        result["number of systems"] = scale  # number
        result["LCOD"] = self.data["LCOD"]
        result["country_idx"] = self.data["country_idx"]

        return result


# battery smoothing
@njit(parallel=True, fastmath=True)
def battery_smoothing(data, dac_cap_ratio, cap_ret, charg_eff, discharg_eff):
    """
    simple battery model to smooth the generation profile of RET.
    numba njit allows quick calculations (100000 runs in ~ 25s).
    :param data: numpy ndarray - generation profile
    :param dac_cap_ratio: float in [0,1] - clip of generation profile (m = capacity of DAC)
    :return: flattened profile and capacity of battery in units of generation profile.
    i.e. if profile in [kW], cap_bat in [kWh]
    """

    m = dac_cap_ratio * cap_ret
    dev = data - m
    soc = np.zeros_like(data)  # state of charge
    diff_profile = np.zeros_like(data)
    # eff = np.array([0, float(charg_eff), float(1 / discharg_eff)])
    soc_t = np.zeros(1)[0]

    for i, x in enumerate(dev):
        if x < 0:
            soc_t += x / discharg_eff
        else:
            soc_t += x * charg_eff
        diff_profile[i] = x
        if soc_t < 0:  # if discharge demand > soc -> no energy left, diff-profile = 0
            soc_t = 0
            diff_profile[i] = 0
        soc[i] = soc_t

    flattened_profile = data - diff_profile
    cap_bat = np.max(soc)
    return flattened_profile, cap_bat


def crf(i, n):
    """
    :param i: interest rate/WACC, e.g. 0.08
    :param n: lifetime of loan, e.g. n = 25 years
    :return: capital recovery factor
    """
    return (i * (1 + i) ** n) / ((1 + i) ** n - 1)


def annualized_costs(capex, cf, wacc, lifetime, *opex):
    """
    function to calculate annualized expenditures of a plant, applicable for RET, HP, bat
    :param capex: specific capex (€/kW)
    :param cf: capacity factor
    :param wacc: interest rate, e.g. 0.07
    :param lifetime: in years
    :param opex: specific opex (€/kW)
    :return:
    """

    if np.isnan(cf).all():
        return np.nan  # cf.__class__(0)

    if len([*opex]) > 1:
        opex_fix, opex_var = np.max([*opex]), np.min([*opex])
        opx = opex_fix + opex_var * cf * 8760  # TODO: check all other opx calculations
    else:
        opx = opex[0]

    return capex * crf(wacc, lifetime) + opx


def lcoe(capex, cf, wacc, lifetime, *opex):
    return annualized_costs(capex, cf, wacc, lifetime, *opex)/ (8760 * cf)


def pso(function, range_1, range_2, pop_size, iterations, function_args):
    """
    2-dimensional particle swarm optimization: the heuristic optimization method takes
    a function to be minimized and the feasible ranges of the 2 input variables as arguments.
    A random set of points (set size = population size) in the feasible region is chosen,
    the function is evaluated at these points and the best value is obtained. For a specified
    number of iterations, the positions get updated by generating a velocity vector which is
    dependent on the distance of each particle to its velocity in the last iteration,
    and to its distance to (a) its personal best and (b) the total best position.

    :param function: function to minimize, here: lcod in CostCalculations class
    :param function_args: list - constant arguments passed to function, e.g. evaluation point
    :param range_1: tuple - range in which the best value for variable 1 shall be found
    :param range_2: tuple - same for second variable
    :param pop_size: int - number of particles
    :param iterations: int - number of iterations to find solution
    :return:
    """

    # define initial random positions of swarm
    pos_1 = np.random.uniform(range_1[0], range_1[1], pop_size)
    pos_2 = np.random.uniform(range_2[0], range_2[1], pop_size)
    pos = np.asarray((pos_1, pos_2)).T

    # initialize particles-wise best position and values
    p_best_val, pos_1, pos_2 = np.array([function(a, b, *function_args)[:3] for a, b in pos]).T
    p_best_pos = np.asarray((pos_1, pos_2)).T

    # get total best
    tot_best_idx = np.argmin(p_best_val)
    tot_best_pos = pos[tot_best_idx]
    tot_best_val = p_best_val[tot_best_idx]

    # initialize velocities
    v = np.zeros((pop_size, 2))

    w = 0.5  # inertia weight
    phi_p = 1  # cognitive coefficient
    phi_g = 1  # social coefficient

    coordinates = []

    # let the particles fly
    for j in range(iterations):
        v = w * v \
            + phi_p * np.random.rand(pop_size, 2) * (p_best_pos - pos) \
            + phi_g * np.random.rand(pop_size, 2) * (tot_best_pos - pos)

        # update all positions and get new values, ensure they stay in feasible region
        pos = pos + v
        pos = np.asarray((np.clip(pos[:, 0], a_min=range_1[0], a_max=range_1[1]),
                          np.clip(pos[:, 1], a_min=range_2[0], a_max=range_2[1]))).T

        # ensure that dac_cap_ratio is always > than e_out_mean (if greater, self.lcod will return changed pos_2)
        values, pos_1, pos_2 = np.array([function(a, b, *function_args)[:3] for a, b in pos]).T
        pos = np.asarray((pos_1, pos_2)).T

        # find out all particles that improved this iteration, set their position and values to individual records
        improved_indices = values < p_best_val
        p_best_pos[improved_indices] = pos[improved_indices]
        p_best_val[improved_indices] = values[improved_indices]

        # update total bests
        tot_best_idx = np.argmin(p_best_val)
        tot_best_pos = p_best_pos[tot_best_idx].copy()
        tot_best_val = p_best_val[tot_best_idx].copy()

        coordinates.append(pos)

    return tot_best_val, tot_best_pos, coordinates


def class_mapper(arrays):
    """
    NOTE: only needed if a classification approach - as opposed to the usual clustering approach - is taken!!


    function to make a common classification/multiindex out of multiple datasets containing discrete class
    values. Tuples of all possible combinations are made and enumerated. Latter number of class-tuples
    is the new classification. Made for classification of data w.r.t. mean temperature/humidity, RET capacity factors
    Arrays must have the same dimension and size.
    e.g. arrays = [[1,2,3,2], [1,2,3,1]], [[0,0,0,0], [0,1,1,0]]
    -> unique classes of arrays: [1,2,3] and [0,1]
    -> possible multiindex classes: [1,0], [1,1], [2,0], [2,1], [3,0], [3,1]
    -> class_dict:  {1: [1,0], 2: [1,1], ...}

    :return: class_dict, reclassified data, i.e. [[1,3,5,3], [1,4,6,1]]
    """

    unique_values = []
    # find all unique values of both arrays
    for arr in arrays:
        unique_values.append(np.unique(arr))

    # compute all possible combinations of them
    mesh = np.meshgrid(*unique_values)

    # define unique class as combination tuple
    classes = [tuple(x) for x in np.asarray([np.ravel(mesh[x]) for x in range(len(mesh))]).T]

    class_dict = dict()
    for class_, class_tuple in enumerate(classes):
        class_dict.update({class_: class_tuple})
    # zip all data arrays to get class tuples at each pixel, replace this tuple with int "multi-class" defined in
    # class_dict.
    result = np.array([[hf.get_key(class_dict, tuple(items)) for items in zip(*rows)]  # zipped elements in rows
                       for rows in zip(*arrays)])  # pixel rows

    return class_dict, np.array(np.where(result == None, np.nan, result), dtype=float)


