# libraries and modules
import geopandas as gpd
import numpy as np
import os
import pandas as pd
from scipy import ndimage
from shapely.geometry import box, Point, LineString, Polygon
import xarray as xr

# own scripts
import helper_functions as hf
import gis
from gis import xlim, ylim, time_range, res
import reclass_dicts


def europe_map(out_path, crs, turkey=True):
    """
    Clip and convert Europe map to study area extent

    :param out_path: str - path where map is stored
    :param crs: int - coordinate reference system (EPSG: xxxx)
    :param turkey: bool - whether Turkey is included or not
    """

    # define study area
    x_lim = xlim[crs]
    y_lim = ylim[crs]

    study_area = gpd.GeoSeries(box(x_lim[0],
                                   y_lim[0],
                                   x_lim[1],
                                   y_lim[1])).set_crs(epsg=crs)

    # clip Europe shapes to study area
    europe = gpd.read_file(rf"raw_data/maps/CNTR_RG_01M_2020_4326.shp").set_crs(4326).to_crs(crs)

    if not turkey:
        europe = europe[europe["CNTR_ID"] != "TR"]

    country_data = pd.read_excel("processed_data/tables/europe_country_specific_data.xlsx").set_index("CNTR_CODE")

    europe = europe[["CNTR_ID", "NAME_ENGL", "geometry"]].set_index("CNTR_ID")
    europe = europe.merge(country_data, right_index=True, left_index=True, how="outer")
    europe = europe[~europe["geometry"].isna()]
    europe = europe.clip(study_area)

    europe.to_file(out_path)


def create_masks(crs, clip_shape):
    """
    create land masks

    input: raw data rasters (.tif)
    output: projected and classified rasters

    """
    # find all masks in raw_data/masks/GSA | other
    rasters = hf.list_full_paths(r"raw_data/masks/other", ".tif", endswith=True)

    # reproject them to template grid of same resolution
    for raster in rasters:
        print(f"Re-projecting raster {raster} ...")
        gis.reproject_raster_to_grid(raster, r"processed_data/masks", target_crs=crs)  # saves masks to given out_dir

    # list of all directories containing reprojected masks
    dirs = hf.list_full_paths(r"processed_data/masks", f"reproj_epsg{crs}")

    # reclassify all reprojected rasters in their respective directories "dirs"
    for reproj_dir in dirs:
        for raster_path in hf.list_full_paths(reproj_dir, ".tif", endswith=True):
            # find raster name
            _, raster_name = os.path.split(raster_path)

            # reclassify
            print(f"Reclassifying raster {raster_name}...")
            raster_reclass = gis.reclassify_raster(raster_path,
                                                   reclass_dicts.superstring_get(raster_name))

            # save backup to respective reclass folder
            sub_str = hf.string_suffix(raster_reclass.res[0], crs)

            # specify raster name, i.e. <rastername>_reclass_epsg...._<res>deg|m.tif
            index = raster_name.find(sub_str)
            out_name = raster_name[:index] + "reclass_" + raster_name[index:]

            # find respective out_dir ([0] -> only item in directory)
            out_reclass_dir = os.path.join("processed_data/masks", f"reclass_{sub_str}")
            if not os.path.isdir(out_reclass_dir):
                os.mkdir(out_reclass_dir)
            gis.write_raster(os.path.join(out_reclass_dir, out_name),
                             raster_reclass.read(),
                             raster_reclass.profile)

            # clip to European land mass
            print(f"Clipping raster {raster_name} to land mass...")
            raster_clip = gis.clip_raster(raster_reclass, clip_shape)

            # save backup of clipped raster
            # specify raster name, i.e. <rastername>_reclass_epsg...._<res>deg|m.tif
            out_name = raster_name[:index] + "reclass_clip_" + raster_name[index:]
            gis.write_raster(os.path.join(out_reclass_dir, out_name),
                             raster_clip.read(),
                             raster_clip.profile)


def land_wind(mask_dir, clip_geom, target_res=res):
    """
    Function takes a directory filled with all reprojected rasters relevant for land suitability of
    wind turbines (must be put there manually!). These rasters get iteratively (highest to lowest resolution) overlaid
    to one raster. This raster gets masked to the shape of European land-mass found in processed_data/maps

    :param mask_dir: directory in which all masks to be considered for wind land availability
    analysis are stored
    :param clip_geom: Geometry of interest (to clip to)
    :param target_res: target resolution to which all masks get agglomerated
    """
    # overlay all raster of specified mask directory
    print("Overlay rasters...")
    gis.raster_overlay(mask_dir,
                       "processed_data/land_availability",
                       f"land_wind_{hf.string_suffix(600)}.tif",
                       overlay_type="multiply")

    # save wind mask with target resolution in land_availability folder
    print(f"Save multiple resolutions of wind mask in {'processed_data/land_availability'}")
    gis.save_multiple_resolutions(rf"processed_data/land_availability/land_wind_{hf.string_suffix(600)}.tif",
                                  r"processed_data/land_availability",
                                  [(10000, 10000), (20000, 20000), target_res],
                                  clip_geom)


def land_pv(mask_dir, clip_geom, target_res=res):
    """
    same as above
    """
    print("Overlay rasters...")
    gis.raster_overlay(mask_dir,
                       "processed_data/land_availability",
                       f"land_pv_{hf.string_suffix(600)}.tif",
                       overlay_type="multiply")

    print(f"Save multiple resolutions of PV mask in {'processed_data/land_availability'}")
    gis.save_multiple_resolutions(os.path.join('processed_data/land_availability',
                                               f"land_pv_{hf.string_suffix(600)}.tif"),
                                  r"processed_data/land_availability",
                                  [(10000, 10000), (20000, 20000), target_res],
                                  clip_geom)


def land_tot(mask_dir, clip_geom, target_res=res):
    """
    calculation of the total amount of land available for either PV or wind
    """
    print("Overlay PV and wind land masks...")
    list_high_res = hf.list_full_paths(mask_dir, f"{hf.string_suffix(600)}.tif", endswith=True)
    gis.raster_overlay(list_high_res,
                       r"processed_data/land_availability",
                       f"land_tot_{hf.string_suffix(600)}.tif",
                       overlay_type="max")

    print("Rescale to target resolution...")
    gis.save_multiple_resolutions(f"processed_data/land_availability/land_tot_{hf.string_suffix(600)}.tif",
                                  r"processed_data/land_availability",
                                  [(10000, 10000), (20000, 20000), target_res],
                                  clip_geom)


def timeseries(clip_shape, target_res=res):
    """
    Reproject/interpolate geospatial timeseries and clip them to a clip shape
    :param clip_shape: .shp file /xr. Dataset,... (see gis.clip_raster), usually Europe shp
    :param target_res: desired resolution of interpolated timeseries
    :return:
    """
    print("loading timeseries data...")
    wind = xr.open_dataset("raw_data/timeseries/wind-onshore-timeseries.nc")
    pv = xr.open_dataset("raw_data/timeseries/open-field-pv-timeseries.nc")
    temp = xr.open_dataset("raw_data/climate_data/tg_ens_mean_0.25deg_reg_v26.0e.nc")
    humid = xr.open_dataset("raw_data/climate_data/hu_ens_mean_0.25deg_reg_v26.0e.nc")

    # study area and grid template
    grid = xr.open_dataset(
        f"processed_data/land_availability/land_tot_{hf.string_suffix(target_res)}.tif")  # projecting to land
    # "humid": humid, "temp": temp,
    for name, item in {"wind": wind, "pv": pv}.items():
        print("processing " + name + " dataset...")
        # preprocess all timeseries. Output saved as .nc in processed_data/timeseries
        if name in ["wind", "pv"]:
            item = gis.timeseries_processing(item,
                                             grid,
                                             target_res=target_res,
                                             time_averaging="hour",
                                             dataset_name=name,
                                             target_crs=3035)
        else:
            item = gis.timeseries_processing(item,
                                             grid,
                                             target_res=target_res,
                                             time_averaging="day",
                                             dataset_name=name,
                                             target_crs=3035)

            item = (item[list(item.data_vars)[0]]  # select band "hu" or "tg" for data access
                    .rio.write_nodata(np.nan)
                    .to_dataset()
                    .rio.interpolate_na(method="nearest")
                    )

        print("clipping " + name + " dataset...")
        item = gis.clip_raster(item, clip_shape)  # clip to European land mass
        item.to_netcdf(
            f"processed_data/timeseries/{name}_reproj_clip_{hf.string_suffix(target_res)}.nc")


def create_xr_dataset(target_res=res):
    """
    function to merge all data to one xarray Dataset. Adds also specific electricity and heat demands [in kWh/tCO2]
    of DAC depending on climatic conditions.
    :param target_res: tuple, target resolution of analysis
    :return: processed dataset
    """
    wind = xr.open_dataset("processed_data/timeseries/wind_reproj_clip.nc")
    pv = xr.open_dataset("processed_data/timeseries/pv_reproj_clip.nc")
    temp = xr.open_dataset("processed_data/timeseries/temp_reproj_clip.nc")
    humid = xr.open_dataset("processed_data/timeseries/humid_reproj_clip.nc")

    climate = (xr.merge([temp, humid])
               .to_dataframe()
               .dropna()
               )

    climate = climate.drop("spatial_ref", axis=1)

    # round to base  5 | 10 for energy requirements calculations (based on Young et al.)
    climate["tg_round"] = climate["tg"].apply(lambda x: hf.custom_round(x, base=5))
    climate["hu_round"] = climate["hu"].apply(lambda x: hf.custom_round(x, base=10))

    # define mapping function of energy requirements dependent on RH and temperature
    input_elec = pd.read_csv("raw_data/tables/DAC_SpecEnergyRequirements_electricity.csv", index_col=0)
    input_heat = pd.read_csv("raw_data/tables/DAC_SpecEnergyRequirements_heat.csv", index_col=0)

    # extent temperature and humidity range by ffill/bfill
    for df in [input_elec, input_heat]:
        df.columns = df.columns.astype(int)
        missing_temperatures = [x for x in range(-40, 40, 5) if x not in df.columns]
        for temperature in missing_temperatures:
            # get the closest temperature in data
            df[temperature] = df[min(df.columns, key=lambda x: abs(x - temperature))]
        df.sort_index(axis=1, inplace=True)

    # find out specific electricity and heat requirements of DAC
    print("calculating specific electricity consumption...")
    climate["DAC_spec_elec_consum"] = climate.apply(lambda x: input_elec[x.tg_round][x.hu_round], axis=1)
    climate["DAC_spec_elec_consum"].attrs["unit"] = "kWh/tCO2"
    print("calculating specific heat consumption...")
    climate["DAC_spec_heat_consum"] = climate.apply(lambda x: input_heat[x.tg_round][x.hu_round], axis=1)
    climate["DAC_spec_heat_consum"].attrs["unit"] = "kWh/tCO2"
    print("calculating specific energy consumption...")
    climate["DAC_spec_energy_consum"] = climate["DAC_spec_elec_consum"] + climate["DAC_spec_heat_consum"]
    climate = climate.to_xarray().transpose()

    land = land_to_xr(target_res[0])

    merged = (xr.merge([climate, pv, wind, land])
              .assign(wind_CF=lambda x: x.wind_power.mean(dim="hourofyear"))
              .assign(pv_CF=lambda x: x.pv_power.mean(dim="hourofyear"))
              )

    hf.store_data(merged, "processed_data/xarray/dataset")

    return merged


def land_to_xr(resolution):
    """ merge all .tif landmasks in one xr Dataset"""
    data = []
    for item in hf.list_full_paths("processed_data/land_availability", f"{resolution}m.tif", endswith=True):
        name = "_".join(os.path.split(item)[1].split("_")[:2])  # e.g., land_pv
        data.append(xr.open_dataset(os.path.join(item)).rename({"band_data": name}))

    land_xr = xr.merge(data).rio.write_crs(3035)
    # hf.store_data(land_xr, "processed_data/xarray/land")

    return land_xr


def ports_to_gdf():
    """ create geodataframe from excel with port coordinated and names """
    europe = gpd.read_file(r"processed_data/maps/europe_clip_3035.shp")
    df = pd.read_excel("raw_data/tables/ports.xlsx")
    df = df[["PORT_NAME", "coords"]].dropna(thresh=2).reset_index(drop=True)
    df["x"] = df["coords"].apply(lambda x: float(x.split(",")[0][2:]))
    df["y"] = df["coords"].apply(lambda x: float(x.split(",")[1][:-1]))
    df = df[["PORT_NAME", "x", "y"]]

    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.x, df.y)).set_crs(4326)
    gdf = gdf.cx[xlim[4326][0]:xlim[4326][1], ylim[4326][0]:ylim[4326][1]].to_crs(3035)
    gdf = gdf.clip(europe.buffer(10000))

    gdf.to_file("processed_data/transport/ports.shp")
    return gdf


def rail_network():
    """
    NOTE: only an attempt to automatically create shape files from georeferenced images by image processing techniques
    (selecting by colors) -> in the end done in ArcGIS Pro
    :return:
    """
    # load raster
    raster = xr.open_dataset("processed_data/transport/rail_network_georeferenced.tif")

    # extract cities (white colored dots)
    cities = xr.where(((raster.sel(band=1) >= 240) & (raster.sel(band=2) >= 240) & (raster.sel(band=3) >= 240)), 1, 0)
    cities_array = cities.band_data.data

    # find connected regions (cities contain of more than one pixel), get centroid
    labeled_array, num_features = ndimage.label(cities_array)
    centroids = ndimage.center_of_mass(cities_array, labeled_array, range(1, num_features + 1))

    # Create a list of shapely Point geometries from the center of mass coordinates
    point_list = [Point(cities.isel(x=int(x), y=int(y)).x,
                        cities.isel(x=int(x), y=int(y)).y)
                  for y, x in centroids]

    rail_stations = gpd.GeoDataFrame(geometry=point_list).set_crs(3035)
    rail_stations.to_file("processed_data/transport/rail_stations.shp")

    # find railways
    colors = [tuple([r, g, b]) for r, g, b in zip(raster.sel(band=1).band_data.data.flatten(),
                                                  raster.sel(band=2).band_data.data.flatten(),
                                                  raster.sel(band=3).band_data.data.flatten())]

    unique_colors, counts = np.unique(colors, return_counts=True, axis=0)





