import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
import pandas as pd
from scipy.fft import fft
from scipy.stats import describe, skew
import sklearn.cluster as sklc
from sklearn.decomposition import PCA
import xarray as xr

import helper_functions as hf


"""
Functions to classify spatial time-series data into similar regions.
Clustering code is rewritten from Calliope 
https://github.com/calliope-project/calliope/blob/main/calliope/time/clustering.py
"""


def create_sample(dataset, size=200):
    stacked = dataset.stack(z=["y", "x"]).dropna(dim="z")
    np.random.seed(42)  # make pseudo-random sample reproducible
    subset = np.random.choice(stacked.z, size, replace=False)
    return stacked.sel(z=subset).unstack().sortby("x").sortby("y", ascending=False)


def preprocess_data(ds, normalization_method, transformation_method):
    print("Preprocessing data...")
    if transformation_method == "fft":
        for v in ds.data_vars:
            time_dim = [item for item in ds[v].coords.dims if "year" in item][0]
            ds[v] = (["z", time_dim], np.abs(fft(ds[v].data, axis=1)))

    elif transformation_method == "PCA":
        for v in ds.data_vars:
            ds[v] = (["z", ...], PCA(n_components=50).fit_transform(ds[v].data))

    elif transformation_method == "sort":
        for v in ds.data_vars:
            time_dim = [item for item in ds[v].coords.dims if "year" in item][0]
            ds[v] = (["z", time_dim], np.sort(ds[v]))

    elif transformation_method in ["statistical_measures", "monthly_mean", "weekly_mean"]:
        time_split = 12  # months
        if transformation_method =="weekly_mean":
            time_split = 52
        for v in ds.data_vars:
            time_dim = [item for item in ds[v].coords.dims if "year" in item][0]
            monthly_intervals = [int(ds[time_dim].size / time_split * j) for j in range(time_split + 1)]

            mean, var, skew, kur, range_ = np.zeros((5, ds.z.size, time_split))  # each has dim No. of sites x months
            measures = {'mean': mean,
                        'variance': var,
                        'skewness': skew,
                        'kurtosis': kur,
                        'range': range_
                        }

            if transformation_method == "monthly_mean":
                measures = {'mean': mean}

            description = np.zeros((ds.z.size, len(measures) * time_split))

            for idx in range(time_split):
                temp_des = describe(ds[v][:, monthly_intervals[idx]:monthly_intervals[idx + 1]], axis=1)
                temp_out = np.array([temp_des._asdict()[item] for item in temp_des._fields[2:]])

                # append min-max tuple separately
                temp_out = np.vstack([temp_out, temp_des.minmax[1] - temp_des.minmax[0]])  # 5 x 9347

                for i, stat in enumerate(measures.keys()):
                    measures[stat][:, idx] = temp_out[i]  # column of length No. of sites

            # normalize data of each respective statistical measure
            for k, stat in enumerate(measures.keys()):
                if (np.max(measures[stat]) - np.min(measures[stat])) != 0:
                    measures[stat] = \
                        (measures[stat] - np.min(measures[stat])) / (np.max(measures[stat]) - np.min(measures[stat]))

                # fill up stats
                description[:, time_split*k: time_split*(k+1)] = measures[stat]  # append array with dim nlocs x months

            ds[v] = (["z", ...], description)

    elif normalization_method == "local_min_max":
        time_dims = [item for item in ds.coords.dims if "year" in item]
        total_min = ds.min(dim=time_dims)
        total_max = ds.max(dim=time_dims)
        ds = (ds - total_min) / (total_max - total_min)

    elif normalization_method == "global_min_max":
        total_min = ds.min()
        total_max = ds.max()
        ds = (ds - total_min) / (total_max - total_min)

    return ds


def reshape_data(data, variables, normalization=None, transformation=None):
    """
    reshape the input DataSet in such way that sklearn cluster methods can be applied.

    :param data: xr.DataSet with coordinates x, y and time (e.g. hourofyear, dayofyear)
    :param variables: variables to be considered in clustering, e.g. pv_power, wind_power, temperature, humidity
    :return: numpy array of shape (No. of sites x cumulative sum of all other dimensions), e.g. (9347 x 8760+8760+365)
    """

    # only consider non-nan sites
    data = data[variables].stack(z=["y", "x"]).dropna(dim="z").transpose("z", ...)  # z = site

    # normalized data:
    if normalization or transformation:
        data = preprocess_data(data, normalization_method=normalization, transformation_method=transformation)
    else:
        print("No normalization/transformation performed")

    # initialize stacked results for clustering
    reshaped_data = np.array([[] for _ in range(data.z.size)])

    # stack data along z-axis
    for v in variables:
        reshaped_data = np.concatenate([reshaped_data, data[v].values], axis=1)

    return np.nan_to_num(reshaped_data, nan=0)


def make_cluster(dataset, variables, n_clusters, method="kmeans", normalization="global_min_max", transformation=None):
    print("Make clusters...")
    X = reshape_data(dataset, variables, normalization, transformation)

    if method == "kmeans":
        clustered_data = sklc.KMeans(n_clusters, n_init=10).fit(X)

    elif method == "hierarchical":
        clustered_data = sklc.AgglomerativeClustering(n_clusters).fit(X)

    stacked = dataset[variables].stack(z=["y", "x"]).dropna(dim="z").transpose("z", ...)
    stacked[f"classes_{method}_{n_clusters}_{transformation}"] = (["z"], clustered_data.labels_)

    return stacked[f"classes_{method}_{n_clusters}_{transformation}"].unstack(), clustered_data


def elbow_method(dataset, variables, kmin, kmax, n_runs, scaling="log", method="kmeans", normalization="global_min_max",
                 transformation="monthly_mean"):
    inertia = []
    clustered_data = []
    if scaling == "log":
        runs = np.unique(np.logspace(np.log10(kmin), np.log10(kmax), n_runs).astype(int))
    else:
        runs = np.linspace(kmin, kmax, n_runs).astype(int)

    for i in runs:
        print(f"Number of clusters: {i}")
        data, model = make_cluster(dataset, variables, n_clusters=i, method=method,
                                   normalization=normalization, transformation=transformation)
        inertia.append(model.inertia_)
        clustered_data.append(data)

    return runs, inertia


def scree_plot_pca(dataset, variables, max_components):
    stacked = dataset[variables].stack(z=["y", "x"]).dropna(dim="z").transpose("z", ...)
    explained_variances = np.zeros((len(variables), max_components))

    for n_components in range(1, max_components + 1):
        for i, v in enumerate(variables):
            pca = PCA(n_components=n_components).fit(stacked[v].data)
            explained_variances[i, n_components - 1] = np.sum(pca.explained_variance_ratio_)

    plt.plot(np.arange(n_components) + 1, explained_variances.T, label=variables)
    plt.legend()
    plt.show()

    return explained_variances


def hartigans_rule(dataset, variables, thresh=10, step=1, normalization="global_min_max", transformation=None):
    X = reshape_data(dataset, variables, normalization, transformation)
    number_of_sites = len(X)
    HK = thresh + 1
    n_clusters = 2

    cluster_list = []
    HK_list = []

    def hk_(inertia_k, inertia_k_1, n_sites, n_clusters):
        return (inertia_k / inertia_k_1 - 1) * (n_sites - n_clusters - 1)

    while HK > thresh and n_clusters < number_of_sites:
        inertia_k = sklc.KMeans(n_clusters, n_init=10).fit(X).inertia_
        inertia_k_1 = sklc.KMeans(n_clusters + 1, n_init=10).fit(X).inertia_

        HK = hk_(inertia_k, inertia_k_1, number_of_sites, n_clusters)
        print(f"n_cluster = {n_clusters}, HK = {HK}")

        HK_list.append(HK)
        cluster_list.append(n_clusters)

        n_clusters += step

        if n_clusters == number_of_sites:
            print("No clear 'ideal' number of clusters")

    return n_clusters - 1, cluster_list, HK_list


def create_control(ds, n_samples):
    ds.data = create_sample(ds.data, n_samples)
    name = "control"

    # real solution = each location one class
    ds.data[name] = ds.data["wind_CF"].where(np.isnan(ds.data["wind_CF"]), 0)  # initialize
    mask = ds.data["wind_CF"].notnull()
    k = 0
    for i, val in np.ndenumerate(mask):
        if val:
            ds.data[name][i] = k
            k += 1

    ds.energy_system_mapper(name, particles=50, iterations=50)
    da = ds.data
    hf.store_data(da, rf"processed_data/xarray/classification_methods/ds_control_{n_samples}_samples")

    return ds


def create_comparison_ds(control_sample, year=2020, cap_ret=10, lr="median", uptake="low", nclusters=200):
    """
    function that creates a dataset containing a subset of final results of classified runs on the
    total dataset to compare it with a control sample of distributed points.

    :param year:
    :param cap_ret:
    :param lr:
    :param uptake:
    :return:
    """

    arrays = []

    if isinstance(control_sample, str):
        control_sample = hf.load_data(control_sample)

    control_sample = control_sample.stack(z=["y", "x"]).dropna(dim="z").rename({"LCOD": "LCOD_control"})["LCOD_control"]
    coords = control_sample.z
    samples = len(coords)
    arrays.append(control_sample)

    paths = hf.list_full_paths("processed_data/xarray", f"ds_{year}_{cap_ret}MW_{lr}_lr_{uptake}_uptake_{nclusters}")

    for path in paths:
        _, name = os.path.split(path)
        name = name[len(f"ds_{year}_{cap_ret}MW_{lr}_lr_{uptake}_uptake_{nclusters}_kmeans_"):]
        da = hf.load_data(path)
        da = da.data.rename({"LCOD": f"LCOD_{name}"})
        da = da[f"LCOD_{name}"]
        da = da.stack(z=["y", "x"]).sel(z=coords)
        arrays.append(da)

    lcod_comparison = xr.merge(arrays)
    hf.store_data(lcod_comparison, f"processed_data/xarray/lcod_comparison_{samples}_samples")

    return lcod_comparison.unstack()


def relative_deviation(dataset, control, approximations=None):
    data = []

    if approximations is None:
        approximations = list(set(dataset.data_vars).difference([control]))

    base = dataset[control].values.flatten()
    base = base[~np.isnan(base)]

    for item in approximations:
        temp_data = dataset[item].values.flatten()
        temp_data = temp_data[~np.isnan(temp_data)]

        data.append((temp_data - base) / base)

    return np.array(data)


def normalize(x):
    ma = np.max(x, axis=-1)
    return (x.T / ma).T
