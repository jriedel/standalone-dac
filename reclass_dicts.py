from math import acos, pi, cos, atan, degrees
import pandas as pd


# convert DN values of slope raster to % slope
def dn_to_slope(dn):
    return float(acos(dn / 250) * 180/pi)


# convert % slope to DN values
def slope_to_dn(slope):
    return float(cos(pi / 180 * slope) * 250)


def superstring_get(raster_name):
    """
    A small function to get the reclassification dictionary
    of a given input raster. As a lot of rasters get reprojected
    and also renamed, this function searches for a substring in the
    raster name which matches a key of the reclass dictionary defined above.
    """
    match = [val for key, val in reclass.items() if key in raster_name]
    return match[0]


# define cut-off slope of 20° for wind and 10° for PV (~ 20%)
# 20% in Colak et al = 11° (int(degrees(atan(0.2))))
dn_cutoff_wind = int(slope_to_dn(20))
dn_cutoff_pv = int(slope_to_dn(10))

# Global solar PV study masks
built_up = {0: 1, 1: 0}
forests = {0: 1, 1: 0}
landcover_cropland = {0: 1, 1: 0}
pop_clusters = {0: 1, 1: 0}
protected_areas = {0: 1, 1: 0}
elevation_60m = {0: 1, 1: 0}
elevation_300m = {0: 1, 1: 0}
waterbodies = {0: 1, 1: 0}

# additional masks:
CLC_wind = pd.read_excel("raw_data/tables/CLC_reclassification.xlsx", index_col=0)["LU_factor_McKenna"]\
    .to_dict()  # other possibilities: "suitability_McKenna", "suitability_FAU", "LU_factor_FAU"
CLC_wind.update({-128: -128})  # nodata

CLC_pv = pd.read_excel("raw_data/tables/CLC_reclassification.xlsx", index_col=0)["LU_factor_Hansen"]\
    .to_dict()  # other possibilities: "suitability_McKenna", "suitability_FAU", "LU_factor_FAU"
CLC_pv.update({-128: -128})  # nodata

DEM_wind = dict.fromkeys(range(0, dn_cutoff_wind), 0) | dict.fromkeys(range(dn_cutoff_wind, 251), 1)
DEM_wind.update({255: -128})  # nodata

DEM_pv = dict.fromkeys(range(0, dn_cutoff_pv), 0) | dict.fromkeys(range(dn_cutoff_pv, 251), 1)
DEM_pv.update({255: -128})  # nodata

# reclassification dict
reclass = {"bulit-up_over50_mask": built_up,
           "forest-compact_mask": forests,
           "landcover_cropland_mask": landcover_cropland,
           "population-clusters_with-buffer25km_mask": pop_clusters,
           "WDPA": protected_areas,
           "terrain_elevation-range-over300m-land_mask": elevation_300m,
           "terrain_elevation-std-over60m-in-30arcsec-pixel-land_mask": elevation_60m,
           "waterbodies_inside-buffer2km_mask": waterbodies,
           "CLC_wind": CLC_wind,
           "CLC_pv": CLC_pv,
           "DEM_wind": DEM_wind,
           "DEM_pv": DEM_pv}

