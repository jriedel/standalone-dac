from collections import defaultdict
import geopandas as gpd
import networkx as nx
import numpy as np
import pandas as pd
import shapely
from shapely.geometry import Point, LineString
from shapely.ops import nearest_points

import gis
import helper_functions as hf
import plotting as pt


cost_params = pd.read_excel("raw_data/tables/transport_cost_params.xlsx", index_col=0)
cost_params.loc["ship_dedicated"] = cost_params.loc["ship_dedicated_7"].values
offset = cost_params["a2"].to_dict()
distance_factor = 1.345  # Euclidean vs. driving distance -> Goncalves et al.

emission_params = pd.read_excel("raw_data/tables/emission_factors.xlsx", index_col=0)


def get_storage_sites(threshold):
    europe = gpd.read_file(r"processed_data/maps/europe_clip_3035.shp")
    storage_sites = gpd.read_file("raw_data/storage_sites/CO2Stop/storage_units.shp").to_crs(3035)
    storage_sites = storage_sites[storage_sites["TOTAL_STOR"] > threshold]
    storage_sites["geometry"] = storage_sites.centroid

    # find offshore index
    storage_sites["offshore"] = 0
    offshore_index = storage_sites[gpd.sjoin(europe, storage_sites, how="right")["index"].isna()].index
    storage_sites.loc[offshore_index, "offshore"] = 1

    storage_sites.reset_index(drop=True, inplace=True)
    storage_sites.index = storage_sites.index + 1000

    return storage_sites


def get_nodes(route_network):
    """
    get nodes of a network
    :param route_network: Geodataframe containing Polylines
    :return: gdf of nodes
    """
    # find start and end points of polyline
    route_network["start_pt"] = route_network["geometry"].apply(lambda x: Point(x.coords[0]))
    route_network["end_pt"] = route_network["geometry"].apply(lambda x: Point(x.coords[-1]))

    # find all unique nodes in network
    nodes = (route_network["end_pt"]
             .union(route_network["start_pt"])
             .explode(index_parts=False)
             .drop_duplicates()
             .reset_index(drop=True)
             )

    return nodes


def transport_cost(distance, mode, method, mass=1):  # distance in m
    """
    calculation of transport cost by mode

    :param distance: in meter!
    :param mode: transport mode as in cost parameters table index
    :param method: "linear", "linear_offset", "fit"
    :param mass: in tons CO2
    :return:
    """
    distance = distance / 1000  # distance in km
    if method == "linear_offset":
        if mode == "ship_dedicated":
            mode = "ship_dedicated_7"
        p = cost_params.loc[mode]
        return p["a2"] + p["a1"] * distance  # distance in km
    elif method == "linear":
        p = cost_params.loc[mode]
        return p["a1"] * distance  # distance in km
    elif method == "fit":
        p = cost_params.loc[mode]
        unitary_cost = p["a1"] + p["a2"] * ((distance + 1) ** p["a3"]) * ((mass + 1) ** p["a4"])  # to avoid 1/0
        return unitary_cost * distance
    else:
        print("No method specified!")


def grey_emissions(distance, mode):
    """ Not included in thesis, but already partly implemented """
    distance = distance / 1000  # km
    return emission_params.loc[mode, "GWP100"] * distance


def network(min_storage_cap, transport_type, year=2020):
    """
    creation of graph, calculation of cost-optimal paths, distances and CO2 emissions

    :param min_storage_cap: float - lower bound cut-off capacity of storage sites to be included (e.g. 1000 Mt)
    :param transport_type: str - in ["dedicated", "container"]
    :param year: int - in [2020, 2030, 2040, 2050]

    :return: geodataframes `storage_sites` and `access_nodes` containing all paths, routes, distances, ...

    """

    # datasets were cleaned in ArcGIS Pro before
    shipping_routes = round_geometries(gpd.read_file("processed_data/transport/shipping_routes_split.shp").set_crs(3035), "geometry", 3)
    rail_routes = round_geometries(gpd.read_file("processed_data/transport/rail_network_new.shp").set_crs(3035), "geometry", 3)
    ports = round_geometries(gpd.read_file("processed_data/transport/ports_new.shp").set_crs(3035), "geometry", 3)
    stations = round_geometries(gpd.read_file("processed_data/transport/rail_stations_new.shp").set_crs(3035), "geometry", 3)
    if year == 2020:
        storage_sites = gpd.read_file("processed_data/transport/Northern_lights.shp").to_crs(3035)
        storage_sites.index = storage_sites.index + 1000
        storage_sites = round_geometries(storage_sites, "geometry", 3)
    else:
        storage_sites = round_geometries(get_storage_sites(min_storage_cap).set_crs(3035), "geometry", 3)

    shipping_nodes, rail_nodes = get_nodes(shipping_routes), get_nodes(rail_routes)

    # all points (ports and offshore storage sites) which are unconnected to shipping network
    unconnected_shipping_points = pd.concat([ports["geometry"],
                                             storage_sites["geometry"][storage_sites["offshore"] == 1]],
                                            ignore_index=True)

    # find next nodes (within shipping routes gdf) to missing access_points (i.e. unconnected ports)
    multi_route_nodes = shipping_nodes.unary_union
    missing_route_points = nearest_points(unconnected_shipping_points, multi_route_nodes)  # returns both start and end

    # create line segments between missing access points and nearest nodes
    new_lines = [LineString([a, b]) for a, b in zip(missing_route_points[0], missing_route_points[1])]
    new_shipping_routes = gpd.GeoDataFrame(data={"start_pt": missing_route_points[0],
                                                 "end_pt": missing_route_points[1]},
                                           geometry=new_lines).set_crs(3035)

    # update shipping routes
    total_shipping_routes = pd.concat([shipping_routes, new_shipping_routes])

    # connect onshore storage sites with transport network via trucking lines
    access_points = pd.concat([stations["geometry"], ports["geometry"]], ignore_index=True)  # only geometries
    onshore_storage_points = storage_sites["geometry"][storage_sites["offshore"] == 0]

    multi_access_points = access_points.unary_union
    missing_segments = nearest_points(onshore_storage_points, multi_access_points)
    new_segments = [LineString([a, b]) for a, b in zip(missing_segments[0], missing_segments[1])]
    trucking_routes = gpd.GeoDataFrame(data={"start_pt": missing_segments[0],
                                             "end_pt": missing_segments[1]},
                                       geometry=new_segments).set_crs(3035)

    # for all transport modes: assign distance and transport mode as edge attributes
    total_shipping_routes["length"] = total_shipping_routes.length
    total_shipping_routes["mode"] = "ship"
    total_shipping_routes["emissions"] = grey_emissions(1000, "ship")
    rail_routes["length"] = rail_routes.length
    rail_routes["mode"] = "rail"
    rail_routes["emissions"] = grey_emissions(1000, "rail")
    trucking_routes["length"] = trucking_routes.length
    trucking_routes["mode"] = "truck"
    trucking_routes["emissions"] = grey_emissions(1000, "truck")  # per km

    # define gdfs of interest
    total_network = pd.concat([rail_routes, total_shipping_routes, trucking_routes])
    ports["type"] = "port"
    stations["type"] = "station"
    access_nodes = pd.concat([ports, stations]).reset_index()

    # reduce precision to avoid disconnected subgraphs:
    total_network = round_geometries(total_network, ["start_pt", "end_pt", "geometry"], 3)
    hf.store_data(total_network, "processed_data/transport/total_network.shp")

    # create network
    netwrk = nx.Graph()
    for idx, row in total_network.iterrows():
        netwrk.add_edge(row["start_pt"],
                        row["end_pt"],
                        length=row["length"],
                        mode=row["mode"],
                        emissions=row["emissions"])

    # solve the rounding problem within network definition by taking the closest nodes
    all_nodes = gpd.GeoDataFrame(geometry=list(netwrk.nodes))
    access_nodes["geometry"] = nearest_points(access_nodes["geometry"], all_nodes.unary_union)[1]
    storage_sites["geometry"] = nearest_points(storage_sites["geometry"], all_nodes.unary_union)[1]

    def weight_function(u, v, data):
        """
        weight function of edges in networkx graph: usually linear weight, besides changes of transport modes
        :param u: start point
        :param v: end point
        :param data: attributes of edges
        :return: cost to travel along edges
        """
        # all edges port - shipping network, u = port coordinate
        if (u, v) in zip(new_shipping_routes["start_pt"], new_shipping_routes["end_pt"]):
            cost = transport_cost(data["length"], f"ship_{transport_type}", "linear_offset")
        # same edges, but ports = receiving nodes -> if rail-connection: add rail offset
        elif (v, u) in zip(new_shipping_routes["start_pt"], new_shipping_routes["end_pt"]) \
                and (v in access_nodes[(access_nodes["type"] == "station")].geometry):
            cost = transport_cost(data["length"], f"ship_{transport_type}", "linear") + offset[f"rail_{transport_type}"]
        # cost-function for all trucking edges (no network, all linear) -> cost(u,v) = cost(v,u)
        elif {u, v} in [set(x) for x in zip(trucking_routes["start_pt"], trucking_routes["end_pt"])]:
            cost = transport_cost(data["length"], f"truck_{transport_type}", "linear_offset")
        else:
            cost = transport_cost(data["length"], f"{data['mode']}_{transport_type}", "linear")
        return cost

    # find the shortest paths from all nodes to each resp. storage site
    storage_site_mapping = {}
    for s in storage_sites.index:
        storage_site_point = storage_sites.loc[s, "geometry"]
        paths = nx.shortest_path(netwrk, target=storage_site_point, weight=weight_function)
        costs = nx.shortest_path_length(netwrk, target=storage_site_point, weight=weight_function)

        # update paths and costs of travelling them to all access nodes
        access_nodes[f"cost_to_{s}"] = access_nodes["geometry"].apply(lambda x: costs.get(x))
        access_nodes[f"path_to_{s}"] = \
            access_nodes["geometry"].apply(lambda x: [point for point in paths.get(x)])

    # find cost-closest storage site
    cost_cols = [col for col in access_nodes.columns if "cost_to" in col]
    access_nodes["minimal_cost"] = np.min(access_nodes[cost_cols], axis=1)
    access_nodes["closest_site"] = access_nodes[cost_cols].apply(lambda x: int(x.idxmin()[8:]), axis=1)
    access_nodes["shortest_path"] = access_nodes.apply(lambda x: x[f"path_to_{x['closest_site']}"], axis=1)

    for mode_ in ["ship", "rail", "truck"]:
        access_nodes[f"{mode_}_path"] = \
            access_nodes["shortest_path"].apply(lambda x: merge_shortest_path(netwrk, x).get(mode_, np.nan))
        access_nodes[f"{mode_}_dist"] = access_nodes[f"{mode_}_path"].dropna().apply(lambda x: x.length)
        access_nodes[f"{mode_}_emissions"] = grey_emissions(access_nodes[f"{mode_}_dist"], mode_)

    dist_cols = [col for col in access_nodes.columns if "_dist" in col]
    emission_cols = [col for col in access_nodes.columns if "_emissions" in col]
    access_nodes["dist_from_node"] = np.sum(access_nodes[dist_cols], axis=1)
    access_nodes["emissions_from_node"] = np.sum(access_nodes[emission_cols], axis=1)

    return storage_sites, access_nodes


def merge_shortest_path(netwrk, points):
    """
    get a list points, check transport mode of connection between the points lump all
    connected paths within each transport mode and return total length and cost per
    transport mode.
    :param netwrk: networkx graph
    :param points:
    :return:
    """

    lumped_paths = defaultdict()
    temp_path = [points[0]]

    mode = netwrk[points[0]][points[1]]["mode"]
    temp_path.append(points[0])
    for u, v in zip(points[:-1], points[1:]):
        edge_attributes = netwrk[u][v]
        if edge_attributes["mode"] == mode:
            temp_path.append(v)

        if (edge_attributes["mode"] != mode) or (v == points[-1]):
            lumped_paths[mode] = LineString(temp_path)
            mode = edge_attributes["mode"]
            temp_path = [v]

    return lumped_paths


def transport_raster(dataset, min_storage_cap, europe, transport_type="dedicated", year=2020, pipeline_phase="dense"):
    """
    convert the discrete costs to access nodes into a spatial distribution (raster) by assuming truck transport to reach
    any access node. Meta function of network() function

    :param dataset: xr.DataSet contained in self.data of instances of class CostCalculations in cost_calculations.py
    :param min_storage_cap:  float - lower bound cut-off capacity of storage sites to be included (e.g. 1000 Mt)
    :param europe: gdf - opened shp file of Europe map
    :param transport_type: "dedicated" or "container"
    :param year: int
    :param pipeline_phase: "dense" or "gas"
    """
    if year == 2020:
        storage_sites, access_nodes = network(min_storage_cap=min_storage_cap,
                                              transport_type=transport_type, year=2020)
    else:
        return transport_raster_2050(dataset, min_storage_cap, europe, pipeline_phase)
    # get raster pixel coords
    coords = gis.xarray_to_gdf(dataset, ["wind_CF"]).dropna().drop("wind_CF", axis=1)
    coords["transport_cost"] = np.inf
    coords["to_node"] = 0
    coords["emissions"] = 0

    for index, row in access_nodes.iterrows():
        # cost to truck CO2 to respective access_node
        cost_to_node = transport_cost(row["geometry"].distance(coords["geometry"]) * distance_factor,
                                      f"truck_{transport_type}",
                                      "linear_offset")
        emissions_to_node = grey_emissions(row["geometry"].distance(coords["geometry"]) * distance_factor, "truck")

        if row["type"] == "station" and not access_nodes["geometry"].duplicated()[index]:  # no "port-stations"
            cost_from_node = row["minimal_cost"] + offset[f"rail_{transport_type}"]  # add rail offset for all "sending" stations
        else:
            cost_from_node = row["minimal_cost"]

        cost_tot = cost_from_node + cost_to_node

        # if transportation to access_node "index" < trucking to all other access_nodes -> go to this node
        coords.loc[cost_tot < coords["transport_cost"], "to_node"] = index
        coords.loc[cost_tot < coords["transport_cost"], "emissions"] = emissions_to_node + row["emissions_from_node"]

        # explicit paths
        coords.loc[cost_tot < coords["transport_cost"], "ship_path"] = row["ship_path"]
        coords.loc[cost_tot < coords["transport_cost"], "rail_path"] = row["rail_path"]
        coords.loc[cost_tot < coords["transport_cost"], "truck_path_to_storage"] = row["truck_path"]
        coords.loc[cost_tot < coords["transport_cost"], "truck_path_to_node"] = \
            coords["geometry"].apply(lambda x: LineString([row["geometry"], x]))

        #  update transport cost = min of all cost "to-date"
        coords.loc[cost_tot < coords["transport_cost"], "transport_cost"] = cost_tot

    # check if direct trucking to storage site is beneficial
    for index, row in storage_sites.iterrows():
        cost_to_site = transport_cost(row["geometry"].distance(coords["geometry"]) * distance_factor,
                                      f"truck_{transport_type}",
                                      "linear_offset")
        emissions_to_site = grey_emissions(row["geometry"].distance(coords["geometry"]) * distance_factor, "truck")
        coords.loc[cost_to_site < coords["transport_cost"], "to_node"] = index
        coords.loc[cost_to_site < coords["transport_cost"], "emissions"] = emissions_to_site

        # paths
        coords.loc[cost_to_site < coords["transport_cost"], "ship_path"] = 0
        coords.loc[cost_to_site < coords["transport_cost"], "rail_path"] = 0
        coords.loc[cost_to_site < coords["transport_cost"], "truck_path_to_storage"] = \
            coords["geometry"].apply(lambda x: LineString([row["geometry"], x]))

        coords.loc[cost_to_site < coords["transport_cost"], "transport_cost"] = cost_to_site

    if year == 2020:
        coords["transport_cost"] += transport_cost(80000, "pipeline_dense_offshore", "fit", 1000000)
        # pipeline transport from Northern Light storage hub to offshore site, generically 1 Mt

    # define catchment areas
    nodes_to_site = dict(zip(access_nodes.index, access_nodes["closest_site"]))
    coords["to_storage"] = coords["to_node"].apply(lambda x: nodes_to_site.get(x, x))

    # define costs and distance by transport mode
    raster = gis.gdf_to_xarray(coords)

    return access_nodes, storage_sites, coords, raster


def transport_raster_2050(dataset, min_storage_cap, europe, pipeline_transport="dense"):
    """
    Pipeline model

    :param dataset: dataset obtained by method CostCalculations .cdr_potential_2()
    :param pipeline_transport: "dense" or "gas"
    :return: raster of transport costs at all possible DAC locations
    """
    storage_sites = get_storage_sites(min_storage_cap)
    coords = gis.xarray_to_gdf(dataset, ["carbon_captured"]).dropna()

    # Euclidean piping
    coords["pipeline_connections"] = coords["geometry"].apply(lambda coord:
                                                              LineString([coord, min(storage_sites["geometry"],
                                                                                     key=lambda point:
                                                                                     coord.distance(point))]))
    coords["pipeline_length"] = coords["pipeline_connections"].length  # m !
    coords["pipeline_idx"] = coords.index

    # onshore part of pipelines
    onshore_pipelines = gpd.overlay(coords.set_geometry("pipeline_connections"), europe, how='intersection')
    onshore_pipelines["onshore_length"] = onshore_pipelines.length
    coords["offshore_length"] = coords.set_geometry("pipeline_connections").length \
                                - onshore_pipelines.groupby("pipeline_idx")["onshore_length"].sum()
    coords["onshore_length"] = coords["pipeline_length"] - coords["offshore_length"]

    coords["onshore_transport_cost"] = transport_cost(coords["onshore_length"],
                                                      f"pipeline_{pipeline_transport}_onshore",
                                                      method="fit",
                                                      mass=1e6)  # Mt to tons
    coords["offshore_transport_cost"] = transport_cost(coords["offshore_length"],
                                                       f"pipeline_{pipeline_transport}_offshore",
                                                       method="fit",
                                                       mass=1e6)  # Mt to tons

    coords["transport_cost"] = coords["onshore_transport_cost"] + coords["offshore_transport_cost"]
    return gis.gdf_to_xarray(coords)


# helper functions
def find_disconnected_subgraphs(graph):
    disconnected_subgraphs = nx.connected_components(graph)

    # Iterate over the disconnected subgraphs
    for subgraph_nodes in disconnected_subgraphs:
        subgraph = graph.subgraph(subgraph_nodes)
        pt.plot_network(subgraph)


def check_nodes(graph, node_gdf):
    nodes = []
    for node in graph.nodes:
        nodes.append(node)

    return node_gdf[~node_gdf["geometry"].isin(nodes)]


def round_geometries(gdf, geom_cols, precision=4):
    """ found to be necessary when creating the graph since some small uncertainties in the processing were found
    to yield disconnected graphs due to not exactly matching coordinates of edges' endpoints"""
    if type(geom_cols) == str:
        geom_cols = [geom_cols]

    for col in geom_cols:
        if type(gdf[col].iloc[0]) == shapely.geometry.linestring.LineString:
            gdf[col] = gdf[col].apply(lambda x: LineString([(np.round(coord[0], precision),
                                                             np.round(coord[1], precision))
                                                            for coord in x.coords]))
        elif type(gdf[col].iloc[0]) == shapely.geometry.point.Point:
            gdf[col] = gdf[col].apply(lambda x: Point((np.round(x.coords[0][0], precision),
                                                       np.round(x.coords[0][1], precision))))

    return gdf


def append_to_line(line, points):
    """ append points to line"""
    if not isinstance(points, list):
        points = [points]

    point_coords = [point.coords for point in points]
    line_coords = [(x, y) for x, y in line.coords]
    updated_line = LineString(line_coords + point_coords)

    return updated_line