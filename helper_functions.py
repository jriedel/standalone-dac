import numpy as np
import importlib
import os
import pickle
import re
import sys


def store_data(data, path):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)


def load_data(path):
    with open(path, 'rb') as file:
        return pickle.load(file)


def list_full_paths(directory, regex_pattern, endswith=False):
    if endswith:
        return [os.path.join(directory, file)
                for file in os.listdir(directory)
                if file.endswith(regex_pattern)]

    return [os.path.join(directory, file)
            for file in os.listdir(directory)
            if re.findall(regex_pattern, file)]


def custom_round(x, base=5):
    if base >= 1:
        return int(base * np.round(x/base))
    if base <= 1:
        return np.round(base * np.round(x/base), int(abs(np.log10(base))+1))


def string_suffix(res, crs=3035):
    if isinstance(res, tuple):
        res = res[0]
    res = round(res, 4) if res < 1 else int(res)
    suffix = "m" if crs == 3035 else "deg"
    return f"epsg{crs}_{res}{suffix}"


def get_key(d, value):
    key = [k for k, v in d.items() if v == value]
    if key:
        return key[0]
    return None


def get_key_from_string(d, string):
    values = []
    for key in d.keys():
        if string in key:
            values.append(d[key])
    return values


def nearest_element(array, value):
    array = np.array(array)
    return array[(np.abs(array - value)).argmin()]


def plot_rgb(array):
    x = np.arange(len(array))
    y = np.ones(len(array))

    for j in range(len(array)):
        plt.scatter(x[j], y[j], color=(np.array(array[j]) / 255))


def reimport(module_name):
    importlib.reload(sys.modules[module_name])
