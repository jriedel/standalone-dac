import geopandas as gpd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colormaps as cm
import pandas as pd
import time
import xarray as xr

import data_processing
import helper_functions as hf
import cost_calculations as cc
import classification as cfc
import plotting as pt
import results as r
import transport_storage as ts


crs = 3035
target_res = (25000, 25000)
storage_cost = 11

if __name__ == '__main__':
    # parameters
    year, cap_ret, lr, tech_uptake = 2020, 10000, "median", "low"
    method, n_clusters, transformation = "kmeans", 200, "fft"
    min_storage_cap = 1000  # Mt

    # define study area
    # data_processing.europe_map("processed_data/maps/europe_clip_3035_wo_Turkey.shp", 3035, turkey=False)
    europe = gpd.read_file(r"processed_data/maps/europe_clip_3035_wo_Turkey.shp")

    # create all masks
    # data_processing.create_masks(3035, europe)

    # find available land
    # data_processing.land_wind("processed_data/masks/wind", europe)
    # data_processing.land_pv("processed_data/masks/pv", europe)
    # data_processing.land_tot("processed_data/land_availability", europe)
    # clc = xr.open_dataset("raw_data/rasters/clc_25000_clip.tif")

    # preprocess climate data and energy timeseries and merge with available land
    # data_processing.timeseries(europe, target_res=target_res)
    # ds = data_processing.create_xr_dataset(target_res=target_res)

    # Second start after preprocessing steps
    # ds = hf.load_data("processed_data/xarray/dataset")  # includes all preprocessed data
    # ds["land_cover"] = clc.band_data
    # ds = cc.CostCalculation(ds, europe, year=year, cap_ret=cap_ret, lr=lr, tech_uptake=tech_uptake)

        ### clustering transformation selection -> fft usually yields best results (see above: transformation = "fft")
        # Elbow method
        # runs, inertia = cfc.elbow_method(ds.data, ["pv_power", "wind_power", "tg", "hu"], 2, 300, 20)

        # Hartigan's rule
        # cluster_data, cluster_list, HF_list = cfc.hartigans_rule(ds.data, ["wind_power", "pv_power", "tg", "hu"], step=5)

        # select and verify classification method
        # control = hf.load_data(rf"processed_data/xarray/classification_methods/ds_control_200_samples")
        # lcod_comparison = cfc.create_comparison_ds(control)
        # pt.violin_plot(lcod_comparison, "LCOD_control", ["LCOD_None",
        #                                                  "LCOD_sort",
        #                                                  "LCOD_statistical_measures",
        #                                                  "LCOD_weekly_mean",
        #                                                  "LCOD_monthly_mean",
        #                                                  "LCOD_fft",
        #                                                  "LCOD_PCA"])

    # apply clustering on all data
    # ds.classify(["pv_power", "wind_power", "tg", "hu"],
    #             method=method,
    #             n_clusters=n_clusters,
    #             normalization="global_min_max",
    #             transformation=transformation)
    # ds.energy_system_mapper(f"classes_{method}_{n_clusters}_{transformation}", particles=50, iterations=40)
    # hf.store_data(ds, rf"processed_data/xarray/ds_{ds.year}_{int(ds.cap_ret/1000)}MW_{lr}_lr_{tech_uptake}_uptake_{n_clusters}_{method}_{transformation}_new")

    ds2020 = hf.load_data(rf"processed_data/xarray/ds_{year}_{int(cap_ret / 1000)}MW_{lr}_lr_{tech_uptake}_uptake_{n_clusters}_{method}_{transformation}")
    ds2050 = hf.load_data(rf"processed_data/xarray/ds_{2050}_{int(100000 / 1000)}MW_{lr}_lr_{tech_uptake}_uptake_{n_clusters}_{method}_{transformation}")
    ds2020 = cc.CostCalculation(ds2020.data, europe, year=year, cap_ret=cap_ret, lr=lr, tech_uptake=tech_uptake)
    ds2050 = cc.CostCalculation(ds2050.data, europe, year=2050, cap_ret=100000, lr=lr, tech_uptake=tech_uptake)

    for item in [ds2020, ds2050]:
        item.rasterize_country_data(["WACC", "country"])
        item.calculate_ret_potential()
        item.system_footprint()

    vec2020 = ds2020.vectorized_lcod()
    vec2050 = ds2050.vectorized_lcod()

    # CDR potential
    cdr = ds2020.cdr_potential_2(0.85)

    # transport cost
    # 2020
    #transport_2020 = ts.transport_raster(ds2020.data, min_storage_cap, europe, year=2020)
    #hf.store_data(transport_2020, "processed_data/xarray/transport_2020")
    access_nodes, storage_sites, coords, raster2020 = hf.load_data("processed_data/xarray/transport_2020")

    # 2050
    # transport_2050 = ts.transport_raster(cdr2050, min_storage_cap, europe, year=2050, pipeline_phase="dense")
    # hf.store_data(transport_2050, "processed_data/xarray/transport_2050")
    raster2050 = hf.load_data("processed_data/xarray/transport_2050")

    vec2020["transport_cost"] = raster2020["transport_cost"]
    vec2050["transport_cost"] = raster2050["transport_cost"]
    vec2020["storage_cost"] = storage_cost
    vec2050["storage_cost"] = storage_cost
    vec2020["specific_cost"] = vec2020["transport_cost"] + vec2020["LCOD"] + storage_cost
    vec2050["specific_cost"] = vec2050["transport_cost"] + vec2050["LCOD"] + storage_cost


